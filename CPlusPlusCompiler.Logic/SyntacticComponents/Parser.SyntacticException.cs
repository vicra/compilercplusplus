﻿using System;
using System.Collections.Generic;

namespace CPlusPlusCompiler.Logic.SyntacticComponents
{
    public partial class Parser
    {
        private void ThrowExpectedTokensException(List<string> list)
        {
            var tokens = "";
            for (var i = 0; i < list.Count; i++)
            {
                tokens += " " + list[i] + " ";
                if (list.Count - 1 > i)
                    tokens += " OR ";
            }
            var previousToken = GetPreviousToken();
            throw new SyntacticException("Expected token(s): " + tokens + " Row: " + previousToken.Row + " Column: " +
                                      previousToken.Column);
        }

        public class SyntacticException : Exception
        {
            public SyntacticException(string errorMessage) : base(errorMessage)
            {
            }
        }
    }
}