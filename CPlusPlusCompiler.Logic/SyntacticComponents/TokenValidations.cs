﻿using System;
using System.Collections.Generic;
using CPlusPlusCompiler.Logic.LexerComponents;

namespace CPlusPlusCompiler.Logic.SyntacticComponents
{
    public static class TokenValidations
    {
        public static bool TokenStartsAsStatement(Token currentToken)
        {
            return TokenStartsAsBasicStatement(currentToken) ||
                   TokenStartsAsDeclarativeStatement(currentToken) ||
                   TokenIsHtmlContent(currentToken);
        }

        public static bool TokenIsHtmlContent(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.HTML_CONTENT
            });
        }

        public static bool TokenStartsAsDeclarativeStatement(Token currentToken)
        {
            return TokenStartsAsFunctionOrVariableDeclaration(currentToken) ||
                   TokenStartsAsStructDeclaration(currentToken) ||
                   TokenStartsAsEnumDeclaration(currentToken) ||
                   TokenStartsAsIncludeStatement(currentToken);
        }

        public static bool TokenStartsAsBasicStatement(Token currentToken)
        {
            return TokenStartsAsExpressionStatement(currentToken) ||
                   TokenStartsAsCompoundStatement(currentToken) ||
                   TokenStartsAsSelectionStatement(currentToken) ||
                   TokenStartsAsIterationStatement(currentToken) ||
                   TokenStartsAsJumpStatement(currentToken) ||
                   TokenStartsAsVariableDeclaration(currentToken) ||
                   TokenIsHtmlContent(currentToken) ||
                   TokenStartsAsLabeledStatement(currentToken);
        }

        public static bool TokenStartsAsBasicStatementForCase(Token currentToken)
        {
            return TokenStartsAsExpressionStatement(currentToken) ||
                   TokenStartsAsCompoundStatement(currentToken) ||
                   TokenStartsAsSelectionStatement(currentToken) ||
                   TokenStartsAsIterationStatement(currentToken) ||
                   TokenStartsAsJumpStatement(currentToken) ||
                   TokenStartsAsVariableDeclaration(currentToken) ||
                   TokenIsHtmlContent(currentToken);
        }

        public static bool TokenStartsAsLabeledStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_CASE,
                TokenTypes.RESERVED_DEFAULT
            });
        }

        public static bool TokenStartsAsVariableDeclaration(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
                   {
                       TokenTypes.RESERVED_CONST
                   }) || TokenIsTypeSpecifier(currentToken);
        }

        public static bool TokenIsTypeSpecifier(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.TYPE_INT,
                TokenTypes.TYPE_CHAR,
                TokenTypes.TYPE_FLOAT,
                TokenTypes.TYPE_STRING,
                TokenTypes.TYPE_DATE,
                TokenTypes.TYPE_BOOLEAN,
                TokenTypes.RESERVED_TRUE,
                TokenTypes.RESERVED_FALSE,
                TokenTypes.TYPE_STRUCT,
                TokenTypes.TYPE_VOID
            });
        }

        public static bool TokenStartsAsJumpStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_BREAK,
                TokenTypes.RESERVED_CONTINUE,
                TokenTypes.RESERVED_RETURN
            });
        }

        public static bool TokenStartsAsIterationStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_WHILE,
                TokenTypes.RESERVED_DO,
                TokenTypes.RESERVED_FOR
            });
        }

        public static bool TokenStartsAsSelectionStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_IF,
                TokenTypes.RESERVED_SWITCH
            });
        }

        public static bool TokenStartsAsCompoundStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.BRACE_LEFT
            });
        }

        public static bool TokenStartsAsExpressionStatement(Token currentToken)
        {
            return TokenStartsAsExpression(currentToken) ||
                   TokenIsEndStatement(currentToken);
        }

        public static bool TokenIsEndStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.END_STATEMENT
            });
        }

        public static bool TokenStartsAsExpression(Token currentToken)
        {
            return TokenStartsAsAssignmentExpression(currentToken);
        }

        public static bool TokenStartsAsAssignmentExpression(Token currentToken)
        {
            return TokenStartsAsPostfixExpressionStatement(currentToken) ||
                   TokenIsArithmeticIncrement(currentToken) ||
                   TokenIsArithmeticDecrement(currentToken) ||
                   TokenIsUnaryOperator(currentToken);
        }

        public static bool TokenIsArithmeticDecrement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ARITHMETIC_DECREMENT
            });
        }

        public static bool TokenIsArithmeticIncrement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ARITHMETIC_INCREMENT
            });
        }

        public static bool TokenIsUnaryOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.AMPERSAND,
                TokenTypes.ARITHMETIC_MULTIPLICATION,
                TokenTypes.ARITHMETIC_ADDITION,
                TokenTypes.ARITHMETIC_SUBSTRACTION,
                TokenTypes.COMPLEMENT,
                TokenTypes.LOGICAL_NOT
            });
        }

        public static bool TokenStartsAsPostfixExpressionStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ID,
                TokenTypes.LITERAL_CHAR,
                TokenTypes.LITERAL_DATE,
                TokenTypes.LITERAL_FLOAT,
                TokenTypes.LITERAL_STRING,
                TokenTypes.Digit,
                TokenTypes.PARENTHESES_LEFT,
                TokenTypes.RESERVED_TRUE,
                TokenTypes.RESERVED_FALSE
            });
        }

		public static bool TokenStartsAsPostfixExpressionPrime(Token currentToken)
		{
			return TokenStartsWithTokens(currentToken, new List<TokenTypes>
			{
				TokenTypes.PARENTHESES_LEFT,
				TokenTypes.ARITHMETIC_INCREMENT,
				TokenTypes.LITERAL_DATE,
				TokenTypes.ARITHMETIC_DECREMENT
			});
		}

		internal static bool TokenIsUnaryExpression(Token currentToken)
		{
			return TokenStartsWithTokens(currentToken, new List<TokenTypes>
			{
				TokenTypes.BITWISE_AND,
				TokenTypes.ARITHMETIC_MULTIPLICATION,
				TokenTypes.ARITHMETIC_ADDITION,
				TokenTypes.ARITHMETIC_SUBSTRACTION,
				TokenTypes.COMPLEMENT,
				TokenTypes.LOGICAL_NOT,
				TokenTypes.Digit,
				TokenTypes.LITERAL_CHAR,
				TokenTypes.LITERAL_FLOAT,
				TokenTypes.LITERAL_STRING,
				TokenTypes.LITERAL_DATE,
				TokenTypes.RESERVED_TRUE,
				TokenTypes.RESERVED_FALSE,
				TokenTypes.ID,
				TokenTypes.PARENTHESES_LEFT,
				TokenTypes.ARITHMETIC_INCREMENT,
				TokenTypes.ARITHMETIC_DECREMENT
			});
		}

		internal static bool TokenIsAddExpressPrime(Token currentToken)
		{
			return TokenStartsWithTokens(currentToken, new List<TokenTypes>
			{
				TokenTypes.ARITHMETIC_ADDITION,
				TokenTypes.ARITHMETIC_SUBSTRACTION,
			});
		}

		internal static bool RelationalExpression(Token currentToken)
		{
			return TokenStartsWithTokens(currentToken, new List<TokenTypes>
			{
				TokenTypes.RELATIONAL_LESS_THAN,
				TokenTypes.RELATIONAL_GREATER_THAN,
				TokenTypes.RELATIONAL_LESS_THAN_OR_EQUAL,
				TokenTypes.RELATIONAL_GREATER_THAN_OR_EQUAL
			});
		}

		internal static bool TokenStartsAsEqualityExpression(Token currentToken)
		{
			return TokenStartsWithTokens(currentToken, new List<TokenTypes>
			{
				TokenTypes.RELATIONAL_EQUALS,
				TokenTypes.RELATIONAL_NOT_EQUALS,
			});
		}

		internal static bool TokenStartsAsAdditiveExpression(Token currentToken)
		{
			return TokenStartsWithTokens(currentToken, new List<TokenTypes>
			{
				TokenTypes.ARITHMETIC_ADDITION,
				TokenTypes.ARITHMETIC_SUBSTRACTION,
			});
		}

        public static bool TokenStartsAsFunctionOrVariableDeclaration(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
                   {
                       TokenTypes.RESERVED_CONST,
                       TokenTypes.TYPE_VOID
                   }) || TokenIsTypeSpecifier(currentToken);
        }

        public static bool TokenStartsAsStructDeclaration(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.TYPE_STRUCT
            });
        }

        public static bool TokenStartsAsEnumDeclaration(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.TYPE_ENUM
            });
        }

        public static bool TokenStartsAsEnumAssigner(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ID
            });
        }

        public static bool TokenStartsAsIncludeStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.HASH_INCLUDE
            });
        }

        public static bool TokenStartsWithTokens(Token token, List<TokenTypes> tokenTypes)
        {
            return tokenTypes.Contains(token.Type);
        }

        public static bool TokenIsIdentifier(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ID
            });
        }

        public static bool TokenIsLiteral(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.Digit,
                TokenTypes.LITERAL_CHAR,
                TokenTypes.LITERAL_FLOAT,
                TokenTypes.LITERAL_STRING,
                TokenTypes.LITERAL_DATE,
                TokenTypes.RESERVED_TRUE,
                TokenTypes.RESERVED_FALSE
            });
        }

        public static bool TokenIsParenthesesLeft(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.PARENTHESES_LEFT
            });
        }

        public static bool TokenIsArithmeticAddition(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ARITHMETIC_ADDITION
            });
        }

        public static bool TokenIsArithmeticSubstraction(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ARITHMETIC_SUBSTRACTION
            });
        }

        public static bool TokenIsComma(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.COMMA
            });
        }

        public static bool TokenIsLogicalOrOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.LOGICAL_OR
            });
        }

        public static bool TokenIsLogicalAndOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.LOGICAL_AND
            });
        }

        public static bool TokenIsBitwiseOrOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.BITWISE_OR
            });
        }

        public static bool TokenIsBitwiseXorOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.BITWISE_XOR
            });
        }

        public static bool TokenIsBitwiseAndOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.BITWISE_AND
            });
        }

        public static bool TokenIsRelationalEquals(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RELATIONAL_EQUALS
            });
        }

        public static bool TokenIsRelationalNotEquals(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RELATIONAL_NOT_EQUALS
            });
        }

        public static bool TokenIsLessThanOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RELATIONAL_LESS_THAN
            });
        }

        public static bool TokenIsGreaterThanOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RELATIONAL_GREATER_THAN
            });
        }

        public static bool TokenIsLessThanOrEqualOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RELATIONAL_LESS_THAN_OR_EQUAL
            });
        }

        public static bool TokenIsGreaterThanOrEqualOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RELATIONAL_GREATER_THAN_OR_EQUAL
            });
        }

        public static bool TokenIsLeftShift(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.BITWISE_LEFT_SHIFT
            });
        }

        public static bool TokenIsRightShift(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.BITWISE_RIGHT_SHIFT
            });
        }

        public static bool TokenIsMultiplicationOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ARITHMETIC_MULTIPLICATION
            });
        }

        public static bool TokenIsDivisionOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ARITHMETIC_DIVISION
            });
        }

        public static bool TokenIsModulusOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ARITHMETIC_MODULUS
            });
        }

        public static bool TokenIsAssignmentOperator(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ASSIGNMENT_EQUAL,
                TokenTypes.ASSIGNMENT_ADD_AND_ASSIGN,
                TokenTypes.ASSIGNMENT_SUBSTRACT_AND_ASSIGN,
                TokenTypes.ASSIGNMENT_MULTIPLY_AND_ASSIGN,
                TokenTypes.ASSIGNMENT_DIVIDE_AND_ASSIGN,
                TokenTypes.ASSIGNMENT_MODULUS_AND_ASSIGN,
                TokenTypes.ASSIGNMENT_BITWISE_AND_ASSIGN,
                TokenTypes.ASSIGNMENT_BITWISE_EXCLUSIVE_OR_AND_ASSIGN,
                TokenTypes.ASSIGNMENT_BITWISE_INCLUSIVE_OR_AND_ASSIGN,
                TokenTypes.ASSIGNMENT_LEFT_SHIFT_AND_ASSIGN,
                TokenTypes.ASSIGNMENT_RIGHT_SHIFT_AND_ASSIGN
            });
        }

        public static bool TokenIsLeftBracket(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.BRACKET_LEFT
            });
        }

        public static bool TokenStartsAsArrayInitializer(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.BRACE_LEFT
            });
        }

        public static bool TokenIsLeftBrace(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.BRACE_LEFT
            });
        }

        public static bool TokenStartsAsIfStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_IF
            });
        }

        public static bool TokenStartsAsSwitchStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_SWITCH
            });
        }

        public static bool TokenStartsAsWhileStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_WHILE
            });
        }

        public static bool TokenStartsAsDoWhileStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_DO
            });
        }

        public static bool TokenStartsAsForStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_FOR
            });
        }

        public static bool TokenIsBreak(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_BREAK
            });
        }

        public static bool TokenIsReturn(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_RETURN
            });
        }

        public static bool TokenIsContinue(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_CONTINUE
            });
        }

        public static bool TokenIsConst(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_CONST
            });
        }

        public static bool TokenIsCase(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_CASE
            });
        }

        public static bool TokenIsDefault(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_DEFAULT
            });
        }

        public static bool TokenIsAssignmentEqual(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ASSIGNMENT_EQUAL
            });
        }

        public static bool TokenIsLiteralInt(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.Digit
            });
        }

        public static bool TokenIsPeriod(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.PERIOD
            });
        }

        public static bool TokeIsArrow(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ARROW
            });
        }

        public static bool TokenIsAccessLink(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ARROW,
                TokenTypes.PERIOD,
                TokenTypes.BRACKET_LEFT
            });
        }

        public static bool TokenIsQuestionMark(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.QUESTION_MARK
            });
        }

        public static bool TokenStartsAsStructCompoundStatement(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.BRACE_LEFT,
                TokenTypes.ID
            });
        }

        public static bool TokenIsHashInclude(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.HASH_INCLUDE
            });
        }

        public static bool TokenIsStruct(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.TYPE_STRUCT
            });
        }

        public static bool TokenStartsAsOptionalIdentifier(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ID,
                TokenTypes.BRACE_LEFT
            });
        }

        public static bool TokenIsParameter(Token currentToken)
        {
            return TokenStartsAsVariableDeclaration(currentToken);
        }

        public static bool TokenIsElse(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.RESERVED_ELSE
            });
        }

		internal static bool TokenStartsAsPrimaryExpression(Token currentToken)
		{
			return TokenStartsWithTokens(currentToken, new List<TokenTypes>
			{
				TokenTypes.LITERAL_CHAR,
				TokenTypes.Digit,
				TokenTypes.LITERAL_DATE,
				TokenTypes.LITERAL_STRING,
				TokenTypes.LITERAL_FLOAT,
				TokenTypes.ID,
				TokenTypes.RESERVED_FALSE,
				TokenTypes.RESERVED_TRUE,
				TokenTypes.PARENTHESES_LEFT
			});
		}

        public static bool TokenIsAccessor(Token currentToken)
        {
            return TokenStartsWithTokens(currentToken, new List<TokenTypes>
            {
                TokenTypes.ARROW,
                TokenTypes.PERIOD,
                TokenTypes.BRACKET_LEFT
            });
        }
    }
}