﻿using System;
using System.Collections.Generic;
using CPlusPlusCompiler.Logic.LexerComponents;
using CPlusPlusCompiler.Logic.TreeComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Literals;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Arithmetic;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Bitwise;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Logical;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Misc;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Relational;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression._Parent;
using CPlusPlusCompiler.Logic.TreeComponents.Statement;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Compound;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.HtmlContent;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Iteration;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Jump;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Labeled;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Selection;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.VariableDeclaration;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements._Parent;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Assignment;
using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id.Accessors;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements;

namespace CPlusPlusCompiler.Logic.SyntacticComponents
{
	public partial class Parser
	{
		private LiteralNode Literal()
		{
			if (TokenValidations.TokenIsLiteral(_currentToken))
			{
				var token = _currentToken;
				GoToNextToken();
				if (token.Type == TokenTypes.Digit)
					return new IntLiteralNode() { Lexeme = token.Lexeme };
				if (token.Type == TokenTypes.LITERAL_CHAR)
					return new CharLiteralNode() { Lexeme = token.Lexeme };
				if (token.Type == TokenTypes.LITERAL_DATE)
					return new DateLiteralNode() { Lexeme = token.Lexeme };
				if (token.Type == TokenTypes.LITERAL_FLOAT)
					return new FloatLiteralNode() { Lexeme = token.Lexeme };
				if (token.Type == TokenTypes.LITERAL_STRING)
					return new StringLiteralNode() { Lexeme = token.Lexeme };
				if ((token.Type == TokenTypes.RESERVED_TRUE)
					|| (token.Type == TokenTypes.RESERVED_FALSE))
					return new BoolLiteralNode() { Lexeme = token.Lexeme };
				return null;
			}
			return null;
		}

		private ExpressionNode AccessLink(ExpressionNode parameter)
		{
		    if (TokenValidations.TokenIsLeftBracket(_currentToken))
		    {
		        GoToNextToken();
		        var expression = Expression();
		        if (_currentToken.Type != TokenTypes.BRACKET_RIGHT)
		            ThrowExpectedTokensException(new List<string> {"]"});
		        GoToNextToken();
                ((IdentifierNode)parameter).Accesors.Add(new IndexAccesorNode()
                {
                    IndexExpression = expression
                });
                return parameter;
		    }
		    if (TokenValidations.TokenIsPeriod(_currentToken))
			{
				GoToNextToken();
				var identifier = Identifier();
                ((IdentifierNode)parameter).Accesors.Add(new PropertyAccesorNode()
                {
                    IdentifierNode = identifier
                });
			    return parameter;
			}
			if (TokenValidations.TokeIsArrow(_currentToken))
			{
				GoToNextToken();
				var identifier = Identifier();
                ((IdentifierNode)parameter).Accesors.Add(new PointerAccesorNode()
                {
                    IdentifierNode = identifier
                });
                return parameter;
			}
			return null;
		}

		#region Enumerators

		private EnumDeclarationNode EnumDeclaration()
		{
		    int row = _currentToken.Row;
			GoToNextToken();
			if (TokenValidations.TokenIsIdentifier(_currentToken))
			{
				//enum assigners o identifier
				var identifier = Identifier();
			    identifier.TypeSpecifier = "enum";
				if (TokenValidations.TokenIsLeftBrace(_currentToken))
				{
					GoToNextToken();
					var enumerators = Enumerators();
					if (_currentToken.Type != TokenTypes.BRACE_RIGHT)
					{
						ThrowExpectedTokensException(new List<string>() { " } " });
					}
					GoToNextToken();
					var enumAssigners = EnumAssigners();
                    if (_currentToken.Type != TokenTypes.END_STATEMENT)
                    {
                        ThrowExpectedTokensException(new List<string>() { " ; " });
                    }
                    GoToNextToken();
                    return new EnumDeclarationNode()
					{
						Identifier = identifier,
						Enumerators = enumerators,
						EnumAssigners = enumAssigners,
                        Row = row
					};
				}
				else if (TokenValidations.TokenIsIdentifier(_currentToken))
				{
					var enumAssigners = EnumAssigners();
					return new EnumDeclarationNode()
					{
						Identifier = identifier,
						EnumAssigners = enumAssigners,
                        Row = row
					};
				}

			}
			else if (TokenValidations.TokenIsLeftBrace(_currentToken))
			{
				GoToNextToken();
				var enumerators = Enumerators();
				if (_currentToken.Type != TokenTypes.BRACE_RIGHT)
				{
					ThrowExpectedTokensException(new List<string>() { " } " });
				}
				GoToNextToken();
				var enumAssigners = EnumAssigners();
                if (_currentToken.Type != TokenTypes.END_STATEMENT)
                {
                    ThrowExpectedTokensException(new List<string>() { " ; " });
                }
                GoToNextToken();
                return new EnumDeclarationNode()
				{
					Enumerators = enumerators,
					EnumAssigners = enumAssigners,
                    Row = row
				};
			}
			return null;
		}

		private List<EnumeratorNode> Enumerators()
		{
			if (TokenValidations.TokenIsIdentifier(_currentToken))
			{
				var enumerator = Enumerator();
				var enumerators = EnumeratorsList();
				enumerators.Insert(0, enumerator);
				return enumerators;
			}
			return new List<EnumeratorNode>();
		}

	    private List<EnumeratorNode> EnumeratorsList()
	    {
            if (TokenValidations.TokenIsComma(_currentToken))
            {
                GoToNextToken();
                var enumerator = Enumerator();
                var enumerators = EnumeratorsList();
                enumerators.Insert(0, enumerator);
                return enumerators;
            }
            return new List<EnumeratorNode>();
        }

	    private List<EnumAssignerNode> EnumAssigners()
		{
			if (TokenValidations.TokenIsIdentifier(_currentToken))
			{
				GoToNextToken();
				var enumAssigner = EnumAssigner();
				var enumAssigners = EnumAssigners();
				enumAssigners.Insert(0, enumAssigner);
				return enumAssigners;
			}
			else
			{
				Epsilon();
				return new List<EnumAssignerNode>();
			}
		}

		private EnumeratorNode Enumerator()
		{
			var identifier = Identifier();
		    if (TokenValidations.TokenIsAssignmentEqual(_currentToken))
		    {
                GoToNextToken();
		    }
            var literal = Literal();
            return new EnumeratorNode() { IdentifierNode = identifier, ValueLiteral = literal };
		}

		private EnumAssignerNode EnumAssigner()
		{
			var identifier = Identifier();
			var valueAssigned = Literal();
			return new EnumAssignerNode() { IdentifierNode = identifier, ValueAssignedNode = valueAssigned };
		}

		//private List<EnumAssignerNode> EnumAssigners()
		//{
		//    if (TokenValidations.TokenIsIdentifier(_currentToken))
		//    {
		//        GoToNextToken();
		//        var enumAssigner = EnumAssigner();
		//        var enumAssigners = EnumAssigners();
		//        enumAssigners.Insert(0, enumAssigner);
		//        return enumAssigners;
		//    }
		//    else
		//    {
		//        Epsilon();
		//        return new List<EnumAssignerNode>();
		//    }
		//}

		//private List<EnumeratorNode> Enumerators()
		//{
		//    if (TokenValidations.TokenIsIdentifier(_currentToken))
		//    {
		//        var enumerator = Enumerator();
		//        var enumerators = Enumerators();
		//        enumerators.Insert(0,enumerator);
		//        return enumerators;
		//    }
		//    else
		//    {
		//        Epsilon();
		//        return new List<EnumeratorNode>();
		//    }

		//}

		//private EnumeratorNode Enumerator()
		//{
		//    if (TokenValidations.TokenIsIdentifier(_currentToken))
		//    {
		//        var identifier = Identifier();
		//        var enumeratorAssign = EnumeratorAssign();
		//        return new EnumeratorNode() { IdentifierNode = identifier, EnumeratorAssignNode = enumeratorAssign };
		//    }
		//    else
		//    {
		//        ThrowExpectedTokensException(new List<string> { "Identifier" });
		//        return null;
		//    }
		//}

		//private EnumeratorAssignNode EnumeratorAssign()
		//{
		//    if (TokenValidations.TokenIsAssignmentEqual(_currentToken))
		//    {
		//        GoToNextToken();
		//        var literal = Literal();
		//        var enumeratorChain = EnumeratorChain();
		//        return new EnumeratorAssignNode { LiteralNode = literal, EnumeratorChainNode = enumeratorChain };
		//    }
		//    else if (TokenValidations.TokenIsComma(_currentToken))
		//    {
		//        var enumeratorChain = EnumeratorChain();
		//        return new EnumeratorAssignNode { EnumeratorChainNode = enumeratorChain };
		//    }
		//    return null;
		//}

		//private EnumeratorChainNode EnumeratorChain()
		//{
		//    if (TokenValidations.TokenIsComma(_currentToken))
		//    {
		//        GoToNextToken();
		//        var enumerator = Enumerator();
		//        return new EnumeratorChainNode() { EnumeratorNode = enumerator };
		//    }
		//    else
		//    {
		//        Epsilon();
		//        return new EnumeratorChainNode();

		//    }
		//}

		//private EnumAssignerNode EnumAssigner()
		//{
		//    if (TokenValidations.TokenIsIdentifier(_currentToken))
		//    {
		//        var identifier = Identifier();
		//        var enumAssignerAssignation = EnumAssignerAssignation();
		//        return new EnumAssignerNode() { IdentifierNode = identifier, EnumAssignerAssignationNode = enumAssignerAssignation };
		//    }
		//    else
		//    {
		//        ThrowExpectedTokensException(new List<string> { "Identifier" });
		//        return null;
		//    }

		//}

		//private EnumAssignerAssignationNode EnumAssignerAssignation()
		//{
		//    if (TokenValidations.TokenIsAssignmentEqual(_currentToken))
		//    {
		//        GoToNextToken();
		//        var valueAssigned = ValueAssigned();
		//        var chain = Chain();
		//        return new EnumAssignerAssignationNode()
		//        {
		//            ValueAssignedNode = valueAssigned, ChainNode = chain
		//        };
		//    }
		//    else if (TokenValidations.TokenIsComma(_currentToken))
		//    {
		//        Chain();
		//        return new EnumAssignerAssignationNode();
		//    }
		//    else
		//    {
		//        ThrowExpectedTokensException(new List<string> { "=", "," });
		//        return null;
		//    }
		//}

		#endregion

		#region  ** Attributes **

		private readonly Lexer _lexer;
		private readonly List<Token> _tokens;
		private Token _currentToken;
		private int _position = -1;

		#endregion

		#region Primary Functions

		public Parser(Lexer lexer)
		{
			_lexer = lexer;
			_tokens = _lexer.GetAllTokens();
            GoToNextToken();
		}

		public List<StatementNode> Parse()
		{
			var code = Program();
			if (_currentToken.Type != TokenTypes.EOF)
				throw new SyntacticException("Expected EOF Token, Row: " + GetPreviousToken().Row);
			return code;
		}

		private List<StatementNode> Program()
		{
			if (TokenValidations.TokenStartsAsStatement(_currentToken))
			{
				var statement = Statement();
				var statementList = StatementList();
				statementList.Insert(0, statement);
				return statementList;
			}
			return null;
		}

		private void Epsilon()
		{
		}

		private void GoToNextToken()
		{
			_currentToken = GetNextToken();
		}

		private Token GetNextToken()
		{
			var myPosition = _position;
			_position++;
			return _tokens[myPosition + 1];
		}

		private Token GetPreviousToken()
		{
			var myPosition = _position;
			_position--;
			return _tokens[myPosition - 1];
		}

		#endregion

		#region Array

		private ArrayInitializerNode ArrayInitializer()
		{
			if (TokenValidations.TokenStartsAsExpression(_currentToken))
			{
				GoToNextToken();
				var expression = Expression();
				return new ArrayInitializerNode { ExpressionNode = expression };
			}
			if (TokenValidations.TokenStartsAsArrayInitializer(_currentToken))
			{
				var arrayInitializerPrime = ArrayInitializerPrime();
				return new ArrayInitializerNode { ArrayInitializerPrimeNode = arrayInitializerPrime };
			}
			ThrowExpectedTokensException(new List<string> { "expression", "array initializer" });
			return null;
		}

		private ArrayInitializerPrimeNode ArrayInitializerPrime()
		{
			if (TokenValidations.TokenStartsAsArrayInitializer(_currentToken))
			{
				GoToNextToken();
				var arrayInitializer = ArrayInitializer();
				if (_currentToken.Type != TokenTypes.BRACE_RIGHT)
					ThrowExpectedTokensException(new List<string> { "}" });
				GoToNextToken();
				var arrayInitializerBiPrime = ArrayInitializerBiPrime();
				return new ArrayInitializerPrimeNode
				{
					ArrayInitializerBiPrimeNode = arrayInitializerBiPrime,
					ArrayInitializerNode = arrayInitializer
				};
			}
			return null;
		}

		private ArrayInitializerBiPrimeNode ArrayInitializerBiPrime()
		{
			if (TokenValidations.TokenIsComma(_currentToken))
			{
				GoToNextToken();
				var arrayInitializerPrime = ArrayInitializerPrime();
				return new ArrayInitializerBiPrimeNode { ArrayInitializerPrimeNode = arrayInitializerPrime };
			}
			Epsilon();
			return new ArrayInitializerBiPrimeNode();
		}

		#endregion

		#region Basic Statements

		private BasicStatementNode BasicStatements()
		{
			if (TokenValidations.TokenStartsAsExpressionStatement(_currentToken))
				return ExpressionStmt();
			if (TokenValidations.TokenStartsAsCompoundStatement(_currentToken))
				return CompoundStatement();
			if (TokenValidations.TokenStartsAsSelectionStatement(_currentToken))
				return SelectionStatement();
			if (TokenValidations.TokenStartsAsIterationStatement(_currentToken))
				return IterationStatement();
			if (TokenValidations.TokenStartsAsJumpStatement(_currentToken))
				return JumpStatement();
			if (TokenValidations.TokenStartsAsVariableDeclaration(_currentToken))
				return VariableDeclarationStatement();
			if (TokenValidations.TokenStartsAsLabeledStatement(_currentToken))
				return LabeledStatement();
			if (TokenValidations.TokenIsHtmlContent(_currentToken))
			{
			    int row = _currentToken.Row;
				var token = _currentToken;
				GoToNextToken();
			    return new HtmlContentStatementNode()
			    {
			        Content = token.Lexeme,
                    Row = row
			    };
			}
			return null;
		}

		private ExpressionStatementNode ExpressionStmt()
		{
            int row = _currentToken.Row;
            if (_currentToken.Type == TokenTypes.END_STATEMENT)
			{
				_currentToken = GetNextToken();
				return new EndStatementNode() {Row = row};
			}
			else
			{

				var exp = Expression();
				if (_currentToken.Type != TokenTypes.END_STATEMENT)
				{
					throw new SyntacticException("Se esperaba un fin de sentencia (;)");
				}
				_currentToken = GetNextToken();
				return new ExpressionStatementNode { ExpressionNode = exp, Row = row};
			}
		}

		private ExpressionNode Expression()
		{
			var assignmentExpression = AssignmentExpression();
			return InlineExpression(assignmentExpression);
		}

		private ExpressionNode InlineExpression(ExpressionNode parameter)
		{
			if (_currentToken.Type == TokenTypes.COMMA)
			{
				_currentToken = GetNextToken();
				var inlineExpressionPrim = InlineExpressionPrime(AssignmentExpression());
				inlineExpressionPrim.AssignmentExpressions.Insert(0, parameter);
				return inlineExpressionPrim;
			}
			else
			{
				return parameter;
			}
		}
		private CompoundStatementNode CompoundStatement()
		{
			if (TokenValidations.TokenIsLeftBrace(_currentToken))
			{
			    int row = _currentToken.Row;
				GoToNextToken();
				var basicStatementsList = BasicStatementsList();
				if (_currentToken.Type != TokenTypes.BRACE_RIGHT)
					ThrowExpectedTokensException(new List<string> { "}" });
				GoToNextToken();
				return new CompoundStatementNode { BasicStatementsList = basicStatementsList, Row = row};
			}
			return null;
		}

		private BasicStatementNode SelectionStatement()
		{
			if (TokenValidations.TokenStartsAsIfStatement(_currentToken))
				return IfStatement();
			if (TokenValidations.TokenStartsAsSwitchStatement(_currentToken))
				return SwitchStatement();
			return null;
		}

		private BasicStatementNode IterationStatement()
		{
			if (TokenValidations.TokenStartsAsWhileStatement(_currentToken))
				return WhileStatement();
			if (TokenValidations.TokenStartsAsDoWhileStatement(_currentToken))
				return DoWhileStatement();
			if (TokenValidations.TokenStartsAsForStatement(_currentToken))
				return ForStatement();
			return null;
		}

		private BasicStatementNode JumpStatement()
		{
			if (TokenValidations.TokenIsBreak(_currentToken))
			{
				GoToNextToken();
				if (_currentToken.Type != TokenTypes.END_STATEMENT)
					ThrowExpectedTokensException(new List<string> { ";" });
				GoToNextToken();
				return new BreakStatementNode();
			}
			if (TokenValidations.TokenIsReturn(_currentToken))
			{
				GoToNextToken();
				var expressionStatementNode = ExpressionStmt();
				return new ReturnStatementNode(expressionStatementNode);
			}
			if (TokenValidations.TokenIsContinue(_currentToken))
			{
				GoToNextToken();
				if (_currentToken.Type != TokenTypes.END_STATEMENT)
					ThrowExpectedTokensException(new List<string> { ";" });
				GoToNextToken();
				return new ContinueStatementNode();
			}
			ThrowExpectedTokensException(new List<string> { "break", "return", "continue" });
			return null;
		}

		private VariableDeclarationStatementNode VariableDeclarationStatement()
		{
			if (TokenValidations.TokenIsTypeSpecifier(_currentToken))
			{
			    int row = _currentToken.Row;
				var typeName = _currentToken.Lexeme;
				GoToNextToken();
				var constList = ConstList();
				var pointerLevel = PointerList();
			    var varDeclarations = VariableDeclarationListOpt();
				if (_currentToken.Type != TokenTypes.END_STATEMENT)
					ThrowExpectedTokensException(new List<string> { ";" });
				GoToNextToken();
			    return new VariableDeclarationStatementNode()
				{
					Type = typeName,
                    IsConst = constList,
                    PointerLevel = pointerLevel,
                    VariableDeclarationList = varDeclarations,
                    Row = row
				};
			}
			if (TokenValidations.TokenIsConst(_currentToken))
			{
                int row = _currentToken.Row;
                var isConst = ConstList();
				var type = TypeSpecifier();
				var pointerLevel = PointerList();
                var varDeclarations = VariableDeclarationListOpt();
                if (_currentToken.Type != TokenTypes.END_STATEMENT)
					ThrowExpectedTokensException(new List<string> { ";" });
				GoToNextToken();
				return new VariableDeclarationStatementNode()
				{
				    PointerLevel = pointerLevel,
                    IsConst = isConst,
                    Type = type,
                    VariableDeclarationList = varDeclarations,
                    Row = row
				};
			}
			return null;
		}

		private LabeledStatementNode LabeledStatement()
		{
		    int row = _currentToken.Row;
			if (TokenValidations.TokenIsCase(_currentToken))
			{
				GoToNextToken();
				var logicalOrExpression = LogicalOrExpression();
				if (_currentToken.Type != TokenTypes.COLON)
					ThrowExpectedTokensException(new List<string> { ":" });
				GoToNextToken();
				var basicStatementsList = BasicStatementsList();
			    return new CaseStatementNode()
			    {
			        LogicalOrExpression = logicalOrExpression,
                    BasicStatementList = basicStatementsList,
                    Row = row
			    };
			}
			if (TokenValidations.TokenIsDefault(_currentToken))
			{
				GoToNextToken();
				if (_currentToken.Type != TokenTypes.COLON)
					ThrowExpectedTokensException(new List<string> { ":" });
				GoToNextToken();
				var basicStatementList = BasicStatementsList();
			    return new DefaultStatementNode()
			    {
			        BasicStatementList = basicStatementList,
			        Row = row
			    };
			}
			return null;
		}

		#endregion

		#region Declaration Statements

		private List<VariableDeclarationNode> VariableDeclarationListOpt()
		{
			if (TokenValidations.TokenIsIdentifier(_currentToken))
			{
				var variableDeclaration = VariableDeclaration();
				var variableDeclarationListOpt = VariableDeclarationListOptConcat();
				variableDeclarationListOpt.Insert(0, variableDeclaration);
				return variableDeclarationListOpt;
			}
			Epsilon();
			return new List<VariableDeclarationNode>();
		}

        private List<VariableDeclarationNode> VariableDeclarationListOptConcat()
        {
            if (TokenValidations.TokenIsComma(_currentToken))
            {
                GoToNextToken();
                var variableDeclaration = VariableDeclaration();
                var variableDeclarationListOpt = VariableDeclarationListOptConcat();
                variableDeclarationListOpt.Insert(0, variableDeclaration);
                return variableDeclarationListOpt;
            }
            Epsilon();
            return new List<VariableDeclarationNode>();
        }



        /// <summary>
        /// valor = 10
        /// valor
        /// valor = valor2
        /// </summary>
        /// <returns></returns>
	    private VariableDeclarationNode VariableDeclaration()
	    {
            var identifier = Identifier();
            if (TokenValidations.TokenIsAssignmentEqual(_currentToken))
            {
                GoToNextToken();
                var expression = AssignmentExpression();
                return new VariableDeclarationAssignmentNode()
                {
                    IdentifierNode = identifier,
                    Value = expression
                };
            }
	        return new OnlyVariableDeclarationNode()
	        {
	            IdentifierNode = identifier
	        };
	    }

	    private IncludeStatementNode IncludeStatement()
		{
		    int row = _currentToken.Row;
			if (TokenValidations.TokenIsHashInclude(_currentToken))
			{
				GoToNextToken();
				if (_currentToken.Type != TokenTypes.LITERAL_STRING)
					ThrowExpectedTokensException(new List<string> { "literal String" });
				var literalString = _currentToken.Lexeme;
				GoToNextToken();
                if (_currentToken.Type != TokenTypes.END_STATEMENT)
                    ThrowExpectedTokensException(new List<string> { ";" });
                GoToNextToken();
                return new IncludeStatementNode
				{
				    IncludePath = new StringLiteralNode() { Lexeme = literalString },
                    Row = row
				};
			}
			return null;
		}

		private StatementNode DeclarativeStatements()
		{
			if (TokenValidations.TokenStartsAsStructDeclaration(_currentToken))
				return StructDeclaration();
			if (TokenValidations.TokenStartsAsFunctionOrVariableDeclaration(_currentToken))
				return FunctionOrVariableDeclaration();
			if (TokenValidations.TokenStartsAsEnumDeclaration(_currentToken))
				return EnumDeclaration();
			if (TokenValidations.TokenStartsAsIncludeStatement(_currentToken))
				return IncludeStatement();
			return null;
		}

		private StructDeclarationNode StructDeclaration()
		{
		    int row = _currentToken.Row;
			GoToNextToken();
			IdentifierNode identifier;
			try
			{
				identifier = Identifier();
				identifier.TypeSpecifier = "struct";
			}
			catch (Exception e)
			{
				string message = "abstract declarator '<anonymous struct>' used as declaration";
				message += '\n' + e.Message;
				throw new SyntacticException(message);
			}

			var structCompoundStatement = StructCompoundStatement();
			if (_currentToken.Type != TokenTypes.END_STATEMENT)
				ThrowExpectedTokensException(new List<string> { ";" });
			GoToNextToken();
			if (structCompoundStatement.IdentifierNode != null)
			{
				structCompoundStatement.IdentifierNode.TypeSpecifier = identifier.Name;
			}
			return new StructDeclarationNode()
			{
                Type = "struct",
				Identifier = identifier,
				StructCompundStatement = structCompoundStatement,
				Row = row
			};
		}
        

		#endregion

		#region Expression

		private InlineExpressionNode InlineExpressionPrime(ExpressionNode param)
		{
			if (_currentToken.Type == TokenTypes.COMMA)
			{
				_currentToken = GetNextToken();
				var inlineExpressionPrime = InlineExpressionPrime(AssignmentExpression());
				inlineExpressionPrime.AssignmentExpressions.Insert(0, param);
				return inlineExpressionPrime;
			}
			else
			{
				var inlineExpression = new InlineExpressionNode();
				inlineExpression.AssignmentExpressions.Insert(0, param);
				return inlineExpression;
			}
		}

		private ExpressionNode AssignmentExpression()
		{
			var logicalOrExpression = LogicalOrExpression();
			var ternaryOperator = TernaryOperator(logicalOrExpression);
			return AssignmentExpressionPrime(ternaryOperator);
		}

		private ExpressionNode AssignmentExpressionPrime(ExpressionNode parameter)
		{
			if (TokenValidations.TokenIsAssignmentOperator(_currentToken))
			{
				_currentToken = GetNextToken();
				return new AssignmentOperator() {ExpressionNode = parameter, AssignmentExpressionNode = AssignmentExpression() } ;
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode TernaryOperator(ExpressionNode parameter)
		{
			if (_currentToken.Type == TokenTypes.QUESTION_MARK)
			{
				_currentToken = GetNextToken();
				var ex = Expression();
				if (_currentToken.Type != TokenTypes.COLON)
				{
					throw new SyntacticException("Colon expected (:)");
				}
				_currentToken = GetNextToken();
				var assignmentExpression = AssignmentExpression();
				return new TernaryOperatorNode(parameter, ex, assignmentExpression);
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode LogicalOrExpression()
		{
			var logicalAndExpression = LogicalAndExpression();
			return LogicalOrExpressionPrime(logicalAndExpression);
		}

		private ExpressionNode LogicalAndExpression()
		{
			var inlcusiveOrExpression = InclusiveOrExpression();
			return LogicalAndExpressionPrime(inlcusiveOrExpression);
		}

		private ExpressionNode InclusiveOrExpression()
		{
			var exclusiveOrExpression = ExclusiveOrExpression();
			return InclusiveOrExpressionPrime(exclusiveOrExpression);
		}

		private ExpressionNode ExclusiveOrExpression()
		{
			var andExpression = AndExpression();
			return ExclusiveOrExpressionPrime(andExpression);
		}

		private ExpressionNode AndExpression()
		{
			var equalityExpression = EqualityExpression();
			return AndExpressionPrime(equalityExpression);
		}

		private ExpressionNode EqualityExpression()
		{
			var relationalExpression = RelationalExpression();
			return EqualityExpressionPrime(relationalExpression);
		}

		private ExpressionNode RelationalExpression()
		{
			var shiftExpression = ShiftExpression();
			return RelationalExpressionPrime(shiftExpression);
		}

		private ExpressionNode ShiftExpression()
		{
			var additiveExpression = AdditiveExpression();
			return ShiftExpressionPrime(additiveExpression);
		}

		private ExpressionNode AdditiveExpression()
		{
			var multiplicativeExpression = MultiplicativeExpression();
			return AdditiveExpressionPrime(multiplicativeExpression);
		}

		private ExpressionNode MultiplicativeExpression()
		{
			var unaryExpression = UnaryExpresion();
			return MultiplicativeExpressionPrime(unaryExpression);
		}

		private ExpressionNode UnaryExpresion()
		{
		    int row = _currentToken.Row;
			if (TokenValidations.TokenStartsAsPrimaryExpression(_currentToken))
			{
				return PostFixExpression();
			}
			else if (_currentToken.Type == TokenTypes.ARITHMETIC_INCREMENT || 
                _currentToken.Type == TokenTypes.ARITHMETIC_DECREMENT || 
                TokenValidations.TokenIsUnaryOperator(_currentToken))
			{
				_currentToken = GetNextToken();
				if (_currentToken.Type == TokenTypes.ARITHMETIC_INCREMENT)
				{
					var unaryExpression = UnaryExpresion();
					return new PrefixIncrementNode() { Expression = unaryExpression};
				}
				else
				{
					var unaryExpression = UnaryExpresion();
					return new PrefixDecrementNode() { Expression = unaryExpression };
				}
			}
			else
			{
				throw new SyntacticException("Expected unary expression");
			}
		}

		private ExpressionNode PostFixExpression()
		{
			var primaryExpression = PrimaryExpression();
			return PostFixExpressionPrime(primaryExpression);
		}

		private ExpressionNode PrimaryExpression()
		{
			if (_currentToken.Type == TokenTypes.ID)
			{
				string identificador = _currentToken.Lexeme;
				_currentToken = GetNextToken();
				return AccessLinkPrimado(new IdentifierNode() { Name = identificador });
			}
			else if (TokenValidations.TokenIsLiteral(_currentToken))
			{
				return Literal();
			}
			else if (_currentToken.Type == TokenTypes.BRACKET_LEFT)
			{
				_currentToken = GetNextToken();
				var exp = Expression();
				if (_currentToken.Type != TokenTypes.PARENTHESES_RIGHT)
				{
					throw new SyntacticException("Expected close of parenthesis");
				}
				_currentToken = GetNextToken();
				return AccessLinkPrimado(exp);
			}
			else
			{
				throw new SyntacticException("Expected primary expression");
			}
		}

		private ExpressionNode AccessLinkPrimado(ExpressionNode parameter)
		{
			if (TokenValidations.TokenIsAccessLink(_currentToken))
			{
				var accessLink = AccessLink(parameter);
				return AccessLinkPrimado(accessLink);
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode PostFixExpressionPrime(ExpressionNode parameter)
		{
			if (TokenValidations.TokenStartsAsPostfixExpressionPrime(_currentToken))
			{
				if (_currentToken.Type == TokenTypes.ARITHMETIC_INCREMENT || 
                    _currentToken.Type == TokenTypes.ARITHMETIC_DECREMENT)
				{
					var tmpCurrentToken = _currentToken;
					_currentToken = GetNextToken();
					if (tmpCurrentToken.Type == TokenTypes.ARITHMETIC_INCREMENT)
					{
						return new PostfixIncrementNode() {Expression = parameter };
					}
					else
					{
						return new PostfixDecrementNode() { Expression = parameter };
					}
				}
				else if(TokenValidations.TokenIsParenthesesLeft(_currentToken))
				{
				    int row = _currentToken.Row;
					_currentToken = GetNextToken();
					//var expression = TokenValidations.TokenIsUnaryExpression(_currentToken) ? Expression() : null;
				    if (((IdentifierNode)parameter).Name == "print")
				    {
				        var param = AssignmentExpression();
                        if (_currentToken.Type != TokenTypes.PARENTHESES_RIGHT)
                        {
                            throw new SyntacticException("Close of parenthesis expected");
                        }
                        _currentToken = GetNextToken();
                        return new PrintStatementNode()
				        {
				            Parameter = param
				        };
				    }
                    if (((IdentifierNode)parameter).Name == "getquerystring")
                    {
                        var param = AssignmentExpression();
                        if (_currentToken.Type != TokenTypes.PARENTHESES_RIGHT)
                        {
                            throw new SyntacticException("Close of parenthesis expected");
                        }
                        _currentToken = GetNextToken();
                        return new GetQueryStringStatementNode()
                        {
                            Parameter = param
                        };
                    }
                    var parameters = FunctionCallParameters();
                    if (_currentToken.Type != TokenTypes.PARENTHESES_RIGHT)
					{
						throw new SyntacticException("Close of parenthesis expected");
					}
					_currentToken = GetNextToken();
				    return new CallFunctionNode()
				    {
				        FunctionName = ((IdentifierNode)parameter).Name,
                        Parameters = parameters,Row = row
                    };
				}
			    return parameter;
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode MultiplicativeExpressionPrime(ExpressionNode parameter)
		{
			if (_currentToken.Type == TokenTypes.ARITHMETIC_MULTIPLICATION || 
			    _currentToken.Type == TokenTypes.ARITHMETIC_DIVISION || 
			    _currentToken.Type == TokenTypes.ARITHMETIC_MODULUS)
			{
				var tmpCurrentToken = _currentToken;
				_currentToken = GetNextToken();
				var unaryExpression = UnaryExpresion();
				var multEx = MultiplicativeExpressionPrime(unaryExpression);
				if (tmpCurrentToken.Type == TokenTypes.ARITHMETIC_MULTIPLICATION)
				{
					return new MultiplicationNode() { LeftNode = parameter, RightNode = multEx};

				}
				else if (tmpCurrentToken.Type == TokenTypes.ARITHMETIC_DIVISION)
				{
					return new DivisionNode() { LeftNode = parameter, RightNode = multEx };
				}
				else
				{
					return new ModulusNode() { LeftNode = parameter, RightNode = multEx };
				}
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode AdditiveExpressionPrime(ExpressionNode parameter)
		{
			Contract.Ensures(Contract.Result<ExpressionNode>() != null);
			if (TokenValidations.TokenStartsAsAdditiveExpression(_currentToken))
			{
				var tmpCurrentToken = _currentToken;
				_currentToken = GetNextToken();
				var mulEx = MultiplicativeExpression();
				var addExp = AdditiveExpressionPrime(mulEx);
				if (tmpCurrentToken.Type == TokenTypes.ARITHMETIC_ADDITION)
				{
					return new AdditionNode() { LeftNode = parameter, RightNode = addExp};

				}
				else
				{
					return new SubstractionNode() { LeftNode = parameter, RightNode = addExp};
				}
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode ShiftExpressionPrime(ExpressionNode parameter)
		{
			if (TokenValidations.TokenIsAddExpressPrime(_currentToken))
			{
				var token = _currentToken;
				_currentToken = GetNextToken();
				var addExp = AdditiveExpression();
				if (token.Type == TokenTypes.BITWISE_LEFT_SHIFT)
				{
					return new BitwiseShiftLeftOperatorNode() {  LeftNode = parameter, RightNode = addExp};
				}
				else
				{
					return new BitwiseShiftRightOperatorNode() { LeftNode = parameter, RightNode = addExp };
				}
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode RelationalExpressionPrime(ExpressionNode parameter)
		{
			if (TokenValidations.RelationalExpression(_currentToken))
			{
				var token = _currentToken;
				_currentToken = GetNextToken();
				var shiEx = ShiftExpression();
				if (token.Type == TokenTypes.RELATIONAL_LESS_THAN)
				{
					return new LessThanOperatorNode() { LeftNode = parameter, RightNode = shiEx };
				}
				else if (token.Type == TokenTypes.RELATIONAL_GREATER_THAN)
				{
					return new GreaterThanOperatorNode() { LeftNode = parameter, RightNode = shiEx };
				}
				else if (token.Type == TokenTypes.RELATIONAL_LESS_THAN_OR_EQUAL)
				{
					return new LessThanOrEqualOperatorNode() { LeftNode = parameter, RightNode = shiEx };
				}
				else
				{
					return new GreaterThanOrEqualOperatorNode() { LeftNode = parameter, RightNode = shiEx };
				}
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode EqualityExpressionPrime(ExpressionNode parameter)
		{
			if (TokenValidations.TokenStartsAsEqualityExpression(_currentToken))
			{
				var tmpCurrentToken = _currentToken;
				_currentToken = GetNextToken();
				var relationalExpression = RelationalExpression();
				if (tmpCurrentToken.Type == TokenTypes.RELATIONAL_EQUALS)
				{
					return new EqualsOperatorNode() {LeftNode = parameter, RightNode = relationalExpression };
				}
				else
				{
					return new EqualsOperatorNode() { LeftNode = parameter, RightNode = relationalExpression };
				}
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode AndExpressionPrime(ExpressionNode parameter)
		{
			if (_currentToken.Type == TokenTypes.BITWISE_AND)
			{
				_currentToken = GetNextToken();
				var equalityExpression = EqualityExpression();
				return new BitwiseAndOperatorNode() {LeftNode = parameter, RightNode = equalityExpression };
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode ExclusiveOrExpressionPrime(ExpressionNode parameter)
		{
			if (_currentToken.Type == TokenTypes.BITWISE_XOR)
			{
				_currentToken = GetNextToken();
				var andExpression = AndExpression();
				return new BitwiseXorOperatorNode() {LeftNode = parameter, RightNode = andExpression };
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode InclusiveOrExpressionPrime(ExpressionNode parameter)
		{
			if (_currentToken.Type == TokenTypes.BITWISE_OR)
			{
				_currentToken = GetNextToken();
				var exclusiveOrExpression = ExclusiveOrExpression();
				return new BitwiseOrOperatorNode() { LeftNode = parameter, RightNode = exclusiveOrExpression};
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode LogicalAndExpressionPrime(ExpressionNode parameter)
		{
			if (_currentToken.Type == TokenTypes.LOGICAL_AND)
			{
				_currentToken = GetNextToken();
				var inclusiveOrExpression = InclusiveOrExpression();
				return new LogicalAndOperatorNode() { LeftNode = parameter, RightNode = inclusiveOrExpression };
			}
			else
			{
				return parameter;
			}
		}

		private ExpressionNode LogicalOrExpressionPrime(ExpressionNode parameter)
		{
			if (_currentToken.Type == TokenTypes.LOGICAL_OR)
			{
				_currentToken = GetNextToken();
				var logicalAndExpression = LogicalAndExpression();
				return new LogicalOrOperatorNode() { LeftNode = parameter, RightNode = logicalAndExpression };
			}
			else
			{
				return parameter;
			}
		}

		#endregion

		#region Identifier

		private IdentifierNode Identifier()
		{
		    int row = _currentToken.Row;
			if (TokenValidations.TokenIsIdentifier(_currentToken))
			{
				var token = _currentToken;
				GoToNextToken();
			    if (TokenValidations.TokenIsAccessor(_currentToken))
			    {
			        var accessorsList = AccessorsList();
                    return new IdentifierNode()
                    {
                        Accesors = accessorsList,
                        Name = token.Lexeme,
                        Row = row
                    };

			    }
				return new IdentifierNode() { Name = token.Lexeme };
			}
			ThrowExpectedTokensException(new List<string> { "Identifier" });
			return null;
		}

	    private List<AccesorNode> AccessorsList()
	    {
            if (TokenValidations.TokenIsAccessor(_currentToken))
            {
                var accessorNode = Accesor();
                var accessorDeclarationList = AccessorsListConcat();
                accessorDeclarationList.Insert(0, accessorNode);
                return accessorDeclarationList;
            }
            Epsilon();
            return new List<AccesorNode>();
        }

        private List<AccesorNode> AccessorsListConcat()
        {
            if (TokenValidations.TokenIsAccessor(_currentToken))
            {
                var accessorNode = Accesor();
                var accessorDeclarationList = AccessorsListConcat();
                accessorDeclarationList.Insert(0, accessorNode);
                return accessorDeclarationList;
            }
            Epsilon();
            return new List<AccesorNode>();
        }

        private AccesorNode Accesor()
        {
            if (_currentToken.Type == TokenTypes.ARROW)
            {
                GoToNextToken();
                var identifier = Identifier();
                return new PointerAccesorNode()
                {
                    IdentifierNode = identifier
                };
            }
            if (_currentToken.Type == TokenTypes.PERIOD)
            {
                GoToNextToken();
                var identifier = Identifier();
                return new PropertyAccesorNode()
                {
                    IdentifierNode = identifier
                };
            }
            if (_currentToken.Type == TokenTypes.BRACKET_LEFT)
            {
                GoToNextToken();
                var expression = LogicalOrExpression();
                if (_currentToken.Type != TokenTypes.BRACKET_RIGHT)
                {
                    throw new SyntacticException(" Expected ]");
                }
                return new IndexAccesorNode()
                {
                    IndexExpression = expression,
                };
            }
            return null;
        }

        private string TypeSpecifier()
		{
			if (TokenValidations.TokenIsTypeSpecifier(_currentToken))
			{
				var token = _currentToken;
				GoToNextToken();
				return token.Lexeme;
			}
			ThrowExpectedTokensException(new List<string> { "BaseType Specifier" });
			return null;
		}

		private bool ConstList()
		{
			if (TokenValidations.TokenIsConst(_currentToken))
			{
				GoToNextToken();
				ConstList();
				return true;
			}
			Epsilon();
			return false;
		}

		private int PointerList()
		{
			if (TokenValidations.TokenIsMultiplicationOperator(_currentToken))
			{
				GoToNextToken();
				return 1 + PointerList();
			}
			return 0;
		}

		#endregion

		#region Parameter

		private List<ParameterNode> Parameters()
		{
			if (TokenValidations.TokenIsParameter(_currentToken))
			{
				var parameter = Parameter();
				var parameterList = ParametersList();
				parameterList.Insert(0, parameter);
				return parameterList;
			}
			Epsilon();
			return new List<ParameterNode>();
		}

	    private List<ExpressionNode> FunctionCallParameters()
        {
	        if (TokenValidations.TokenStartsAsAssignmentExpression(_currentToken))
            {
                var parameter = FunctionCallParameter();
                var parameterList = FunctionCallParametersList();
                parameterList.Insert(0, parameter);
                return parameterList;
            }
            Epsilon();
			return new List<ExpressionNode>();
	    }

	    private List<ExpressionNode> FunctionCallParametersList()
	    {
            if (TokenValidations.TokenIsComma(_currentToken))
            {
                GoToNextToken();
                var parameter = FunctionCallParameter();
                var parameterList = FunctionCallParametersList();
                parameterList.Insert(0, parameter);
                return parameterList;
            }
            Epsilon();
            return new List<ExpressionNode>();
        }

	    private ExpressionNode FunctionCallParameter()
	    {
	        return AssignmentExpression();
	    }


	    private List<ParameterNode> ParametersList()
		{
			if (TokenValidations.TokenIsComma(_currentToken))
			{
				GoToNextToken();
				var parameter = Parameter();
				var parameterList = ParametersList();
				parameterList.Insert(0, parameter);
				return parameterList;
			}
			Epsilon();
			return new List<ParameterNode>();
		}

		private ParameterNode Parameter()
		{
			if (TokenValidations.TokenIsTypeSpecifier(_currentToken))
			{
				var type = _currentToken.Lexeme;
				if (_currentToken.Type == TokenTypes.TYPE_STRUCT)
				{
					GoToNextToken();
					var typeSpecifier = Identifier();
					var constList = ConstList();
					var pointerLevel = PointerList();
					var identifier = Identifier();
					identifier.IsConst = constList;
					identifier.PointerLevel = pointerLevel;
					identifier.TypeSpecifier = typeSpecifier.Name;
					return new StructParameterNode
					{
						TypeSpecifier = typeSpecifier,
						Identifier = identifier
					};
				}
				else
				{
					GoToNextToken();
					var constList = ConstList();
					var pointerLevel = PointerList();
					var identifier = Identifier();
					identifier.IsConst = constList;
					identifier.PointerLevel = pointerLevel;
					identifier.TypeSpecifier = type;
					return new ParameterNode
					{
					    Identifier = identifier
					};
				}
			}
			if (TokenValidations.TokenIsConst(_currentToken))
			{
				GoToNextToken();
				var constLst = ConstList();
				var type = TypeSpecifier();
				var pointerLevel = PointerList();
				var identifier = Identifier();
				identifier.IsConst = constLst;
				identifier.IsConst = true;
				identifier.TypeSpecifier = type;
				identifier.PointerLevel = pointerLevel;

				return new ParameterNode() { Identifier = identifier };
			}
			return null;
		}

		#endregion

		#region Statements

		private List<StatementNode> StatementList()
		{
			if (TokenValidations.TokenStartsAsStatement(_currentToken))
			{
				var statement = Statement();
				var statementList = StatementList();
				statementList.Insert(0, statement);
				return statementList;
			}
			Epsilon();
			return new List<StatementNode>();
		}

		private StatementNode Statement()
		{
			if (TokenValidations.TokenStartsAsDeclarativeStatement(_currentToken))
				return DeclarativeStatements();
			if (TokenValidations.TokenStartsAsBasicStatement(_currentToken))
				return BasicStatements();
			if (TokenValidations.TokenIsHtmlContent(_currentToken))
			{
			    int row = _currentToken.Row;
				var token = _currentToken;
				GoToNextToken();
				return new HtmlContentStatementNode()
				{
				    Content = token.Lexeme,
                    Row = row
                };
			}
			return null;
		}

		private StructCompundStatementNode StructCompoundStatement()
		{
			if (TokenValidations.TokenIsLeftBrace(_currentToken))
			{
				GoToNextToken();
				var variableDeclarationListOpt = VariableDeclarationStatementsList();
				if (_currentToken.Type != TokenTypes.BRACE_RIGHT)
					ThrowExpectedTokensException(new List<string> { "}" });
				GoToNextToken();
				if (TokenValidations.TokenIsIdentifier(_currentToken))
				{
					var initializesStructs = StructsOrInitializedStructs();
					return new StructCompundStatementNode
					{
						VariableDeclarationListOpt = variableDeclarationListOpt,
						InitializedStructNodes = initializesStructs
					};
				}
				return new StructCompundStatementNode
				{
					VariableDeclarationListOpt = variableDeclarationListOpt
				};
			}
			if (TokenValidations.TokenIsIdentifier(_currentToken))
			{
				var identifier = Identifier();
				return new StructCompundStatementNode
				{
					IdentifierNode = identifier
				};
			}
			Epsilon();
			return new StructCompundStatementNode();
		}

	    private List<VariableDeclarationStatementNode> VariableDeclarationStatementsList()
	    {
            if (TokenValidations.TokenStartsAsBasicStatement(_currentToken))
            {
                var variableDeclaration = VariableDeclarationStatement();
                var variableDeclarationStatementsList = VariableDeclarationStatementsList();
                variableDeclarationStatementsList.Insert(0, variableDeclaration);
                return variableDeclarationStatementsList;
            }
            Epsilon();
	        return new List<VariableDeclarationStatementNode>();
	    }


        private List<BasicStatementNode> BasicStatementsList()
		{
			if (TokenValidations.TokenStartsAsBasicStatement(_currentToken))
			{
				var basicStatement = BasicStatements();
				var basicStatementsList = BasicStatementsList();
				basicStatementsList.Insert(0, basicStatement);
				return basicStatementsList;
			}
			Epsilon();
			return new List<BasicStatementNode>();
		}

		private WhileStatementNode WhileStatement()
		{
		    int row = _currentToken.Row;
			GoToNextToken();
			if (_currentToken.Type != TokenTypes.PARENTHESES_LEFT)
				ThrowExpectedTokensException(new List<string> { "(" });
			GoToNextToken();
			var expression = Expression();
			if (_currentToken.Type != TokenTypes.PARENTHESES_RIGHT)
				ThrowExpectedTokensException(new List<string> { ")" });
			GoToNextToken();
			var basicStatements = BasicStatements();
		    return new WhileStatementNode()
		    {
		        Condition = expression,
		        BasicStatement = basicStatements,
		        Row = row
		    };
		}

		private DoWhileStatementNode DoWhileStatement()
		{
            int row = _currentToken.Row;
            GoToNextToken();
			var basicStatements = BasicStatements();
			if (_currentToken.Type != TokenTypes.RESERVED_WHILE)
				ThrowExpectedTokensException(new List<string> { "while" });
			GoToNextToken();
			if (_currentToken.Type != TokenTypes.PARENTHESES_LEFT)
				ThrowExpectedTokensException(new List<string> { "(" });
			GoToNextToken();
			var expression = Expression();
			if (_currentToken.Type != TokenTypes.PARENTHESES_RIGHT)
				ThrowExpectedTokensException(new List<string> { ")" });
			GoToNextToken();
			if (_currentToken.Type != TokenTypes.END_STATEMENT)
				ThrowExpectedTokensException(new List<string> { ";" });
			GoToNextToken();
		    return new DoWhileStatementNode()
		    {
		        BasicStatement = basicStatements,
		        Condition = expression,
		        Row = row
		    };
		}

		private BasicStatementNode ForStatement()
		{
			GoToNextToken();
			if (_currentToken.Type != TokenTypes.PARENTHESES_LEFT)
				ThrowExpectedTokensException(new List<string> { "(" });
			GoToNextToken();
			var forExpressionStatement = ForExpressionStatement();
			if (_currentToken.Type != TokenTypes.PARENTHESES_RIGHT)
				ThrowExpectedTokensException(new List<string> { ")" });
			GoToNextToken();
			var basicStatements = BasicStatements();
            if (((ForExpressionStatementNode)forExpressionStatement).ExpressionA != null)
            {
                return new ForStatementNode()
                {
                    BasicStatement = basicStatements,
                    ForExpressionStatement = forExpressionStatement
                };
            }
			return new ForEachStatementNode()
			{
				BasicStatement = basicStatements,
				ForExpressionStatement = forExpressionStatement
			};
		}

		private ForExpressionStatementNode ForExpressionStatement()
		{
			if (TokenValidations.TokenStartsAsExpression(_currentToken))
			{
				var expressionA = AssignmentExpression();
				if (_currentToken.Type != TokenTypes.END_STATEMENT)
					ThrowExpectedTokensException(new List<string> { ";" });
				GoToNextToken();
				var expressionB = LogicalOrExpression();
				if (_currentToken.Type != TokenTypes.END_STATEMENT)
					ThrowExpectedTokensException(new List<string> { ";" });
				GoToNextToken();
				var expressionC = UnaryExpresion();
				return new ForExpressionStatementNode() 
				{ 
					ExpressionA = expressionA, 
					ExpressionB = (BinaryOperatorNode)expressionB, 
					ExpressionC = (UnaryOperatorNode)expressionC
				};
			}
			else if (TokenValidations.TokenIsTypeSpecifier(_currentToken))
			{
				var tokenType = _currentToken.Lexeme;
				GoToNextToken();
				var pointerLevel = PointerList();
				var leftIdentifier = Identifier();
				leftIdentifier.TypeSpecifier = tokenType;
				leftIdentifier.PointerLevel = pointerLevel;

				if (_currentToken.Type != TokenTypes.COLON)
					ThrowExpectedTokensException(new List<string> { ":" });
				GoToNextToken();
				var rightIdentifier = Identifier();
				return new ForExpressionStatementNode() 
				{ 
					LeftIdentifier = leftIdentifier, 
					RightIdentifier = rightIdentifier 
				};
			}
			return null;
		}

		private SwitchStatementNode SwitchStatement()
		{
		    int row = _currentToken.Row;
			GoToNextToken();
			if (_currentToken.Type != TokenTypes.PARENTHESES_LEFT)
				ThrowExpectedTokensException(new List<string> { "(" });
			GoToNextToken();
			var expression = Expression();
			var switchStatementNode = new SwitchStatementNode { Expression = expression };
			if (_currentToken.Type != TokenTypes.PARENTHESES_RIGHT)
				ThrowExpectedTokensException(new List<string> { ")" });
			GoToNextToken();
			var basicStatements = BasicStatements();
			switchStatementNode.BasicStatement = basicStatements;
		    switchStatementNode.Row = row;
            return switchStatementNode;
		}

		private IfStatementNode IfStatement()
		{
		    int row = _currentToken.Row;
			GoToNextToken();
			if (_currentToken.Type != TokenTypes.PARENTHESES_LEFT)
				ThrowExpectedTokensException(new List<string> { "(" });
			GoToNextToken();
			var condition = Expression();
			var ifNode = new IfStatementNode { Condition = condition };
			if (_currentToken.Type != TokenTypes.PARENTHESES_RIGHT)
				ThrowExpectedTokensException(new List<string> { ")" });
			GoToNextToken();
			var basicStatements = BasicStatements();
			ifNode.BasicStatement = basicStatements;
			if (TokenValidations.TokenIsElse(_currentToken))
			{
				GoToNextToken();
				var elseStatements = BasicStatements();
				ifNode.Else = elseStatements;
			}
		    ifNode.Row = row;
			return ifNode;
		}

		private StatementNode FunctionOrVariableDeclaration()
		{
            int row = _currentToken.Row;
            if (TokenValidations.TokenIsTypeSpecifier(_currentToken))
			{
				IdentifierNode identifier;
				if (TokenValidations.TokenIsStruct(_currentToken))
				{
					return StructDeclarationNode();
				}
				var type = _currentToken.Lexeme;
				GoToNextToken();
				var isConst = ConstList();
				var pointerLevel = PointerList();

			    var nextToken = _tokens[_position+1];

                if (TokenValidations.TokenIsParenthesesLeft(nextToken))
				{
                    identifier = Identifier();
                    identifier.IsConst = isConst;
                    identifier.PointerLevel = pointerLevel;
                    identifier.TypeSpecifier = type;
                    return FunctionDeclaration(identifier);
				}
			    var varDeclarationNode = VariableDeclarationListOpt();
                if (_currentToken.Type != TokenTypes.END_STATEMENT)
                    ThrowExpectedTokensException(new List<string> { ";" });
                GoToNextToken();
                return new VariableDeclarationStatementNode()
                {
                    Type = type,
                    IsConst = isConst,
                    PointerLevel = pointerLevel,
                    VariableDeclarationList = varDeclarationNode,
                    Row = row
                };

       //         if (TokenValidations.TokenIsEndStatement(nextToken))
			    //{
       //             identifier = Identifier();
       //             identifier.IsConst = isConst;
       //             identifier.PointerLevel = pointerLevel;
       //             identifier.TypeSpecifier = type;
			    //    return new VariableDeclarationStatementNode()
			    //    {
       //                 VariableDeclarationList = new List<VariableDeclarationNode>()
       //                 {
       //                     new OnlyVariableDeclarationNode()
       //                     {
       //                         IdentifierNode = identifier
       //                     }
       //                 },
       //                 Row = row,
       //                 Type = type
       //             };
       //         }
       //         else if (TokenValidations.TokenIsAssignmentEqual(nextToken))
			    //{
       //             identifier = Identifier();
       //             identifier.IsConst = isConst;
       //             identifier.PointerLevel = pointerLevel;
       //             identifier.TypeSpecifier = type;
			    //    if (TokenValidations.TokenStartsAsAssignmentExpression(_currentToken))
			    //    {
			    //        var assignmentExpression = AssignmentExpression();
			    //        if (TokenValidations.TokenIsEndStatement(_currentToken))
			    //        {
       //                     GoToNextToken();
			    //            return new VariableDeclarationStatementNode()
			    //            {
       //                         VariableDeclarationList = new List<VariableDeclarationNode>()
       //                         {
       //                             new VariableDeclarationAssignmentNode()
       //                             {
       //                                 IdentifierNode = identifier,
       //                                 Value = assignmentExpression
       //                             }
       //                         },
       //                         Row = row,
       //                         Type = type
       //                     };
       //                 }
			    //        ThrowExpectedTokensException(new List<string>() {" ; "});
			    //    }
       //             else if (TokenValidations.TokenIsLeftBrace(_currentToken))
			    //    {
			            
			    //    }
			    //}
			}
			if (TokenValidations.TokenIsConst(_currentToken))
			{
				GoToNextToken();
				if (TokenValidations.TokenIsStruct(_currentToken))
				{
					GoToNextToken();
					var structCompound = StructCompoundStatement();
					var instanceList = InitializedStructs();
					if (_currentToken.Type != TokenTypes.END_STATEMENT)
					{
						ThrowExpectedTokensException(new List<string>() { " ; " });
					}
					GoToNextToken();
					return new ConstStructDeclarationtNode()
					{
						StructCompundStatement = structCompound,
						InitializedStructs = instanceList
					};
				}
			    var isConst = ConstList();
				var type = TypeSpecifier();
				var pointerLevel = PointerList();
				var identifier = Identifier();
				identifier.TypeSpecifier = type;
				identifier.PointerLevel = pointerLevel;
				identifier.IsConst = isConst;
			    identifier.IsConst = true;

                if (TokenValidations.TokenIsAssignmentEqual(_currentToken))
                {
                    GoToNextToken();
                    if (TokenValidations.TokenStartsAsAssignmentExpression(_currentToken))
                    {
                        var assignmentExpression = AssignmentExpression();
                        if (TokenValidations.TokenIsEndStatement(_currentToken))
                        {
                            GoToNextToken();
                            return new VariableDeclarationStatementNode()
                            {
                                VariableDeclarationList = new List<VariableDeclarationNode>()
                                {
                                    new OnlyVariableDeclarationNode()
                                    {
                                        IdentifierNode = identifier
                                    }
                                },
                                Row = row
                            };
                        }
                        ThrowExpectedTokensException(new List<string>() { " ; " });
                    }
                    else if (TokenValidations.TokenIsLeftBrace(_currentToken))
                    {

                    }
                }
				return new VariableDeclarationStatementNode()
				{
                    VariableDeclarationList = new List<VariableDeclarationNode>()
                    {
                        new OnlyVariableDeclarationNode()
                        {
                            IdentifierNode = identifier
                        }
                    },
                    Row = row
                };
			}
			return null;
		}

	    private StatementNode FunctionDeclaration(IdentifierNode identifier)
	    {
	        int row = _currentToken.Row;
	        GoToNextToken();
	        var parameters = Parameters();
	        if (_currentToken.Type != TokenTypes.PARENTHESES_RIGHT)
	            ThrowExpectedTokensException(new List<string> {")"});
	        GoToNextToken();
	        var compoundStatement = CompoundStatement();
	        return new FunctionDeclarationNode
	        {
	            Parameters = parameters,
	            CompoundStatement = compoundStatement,
	            FunctionName = identifier.Name,
	            Type = identifier.TypeSpecifier,
                Row = row
	        };
	    }

	    private StatementNode StructDeclarationNode()
	    {
	        IdentifierNode identifier;
	        GoToNextToken();
	        identifier = Identifier();
	        identifier.TypeSpecifier = "struct";
	        var structCompound = StructCompoundStatement();
	        return new StructDeclarationNode()
	        {
	            Identifier = identifier,
	            StructCompundStatement = structCompound
	        };
	    }

	    public List<InitializedStructInstanceNode> InitializedStructs()
		{
			if (TokenValidations.TokenStartsAsExpression(_currentToken))
			{
				var initializedStruct = InitializedStruct();
				var initializedStructs = InitializedStructs();
				initializedStructs.Insert(0, initializedStruct);
				return initializedStructs;
			}
			return new List<InitializedStructInstanceNode>();
		}

		public List<StructInstanceNode> StructsOrInitializedStructs()
		{
			if (TokenValidations.TokenStartsAsExpression(_currentToken))
			{
				var initializedStruct = StructOrInitializedStruct();
				var initializedStructs = StructsOrInitializedStructsList();
				initializedStructs.Insert(0, initializedStruct);
				return initializedStructs;
			}
			return new List<StructInstanceNode>();
		}

		public List<StructInstanceNode> StructsOrInitializedStructsList()
		{
			if (TokenValidations.TokenIsComma(_currentToken))
			{
				GoToNextToken();
				var initializedStruct = StructOrInitializedStruct();
				var initializedStructs = StructsOrInitializedStructsList();
				initializedStructs.Insert(0, initializedStruct);
				return initializedStructs;
			}
			return new List<StructInstanceNode>();
		}

		public InitializedStructInstanceNode InitializedStruct()
		{
			var identifier = Identifier();
			if (_currentToken.Type != TokenTypes.ASSIGNMENT_EQUAL)
			{
				ThrowExpectedTokensException(new List<string>() { " = " });
			}
			GoToNextToken();
			if (_currentToken.Type != TokenTypes.BRACE_LEFT)
			{
				ThrowExpectedTokensException(new List<string>() { " { " });
			}
			GoToNextToken();
			var literalNodes = LiteralNodes();
			if (_currentToken.Type != TokenTypes.BRACE_RIGHT)
			{
				ThrowExpectedTokensException(new List<string>() { " } " });
			}
			GoToNextToken();
			return new InitializedStructInstanceNode()
			{
				IdentifierNode = identifier,
				LiteralNodes = literalNodes
			};
		}
		public StructInstanceNode StructOrInitializedStruct()
		{
			var identifier = Identifier();
			if (_currentToken.Type == TokenTypes.ASSIGNMENT_EQUAL)
			{
				GoToNextToken();
				if (_currentToken.Type == TokenTypes.BRACE_LEFT)
				{
					GoToNextToken();
					var literalNodes = LiteralNodes();
					if (_currentToken.Type == TokenTypes.BRACE_RIGHT)
					{
						GoToNextToken();
						return new InitializedStructInstanceNode()
						{
							IdentifierNode = identifier,
							LiteralNodes = literalNodes
						};
					}
					ThrowExpectedTokensException(new List<string>() { " } " });
				}
				ThrowExpectedTokensException(new List<string>() { " { " });
			}
			return new StructInstanceNode() { IdentifierNode = identifier };
		}

		private List<ExpressionNode> LiteralNodes()
		{
			if (TokenValidations.TokenIsLiteral(_currentToken))
			{
				var literal = Literal();
				var literalList = LiteralNodesList();
				literalList.Insert(0, literal);
				return literalList;
			}
			return new List<ExpressionNode>();
		}

		private List<ExpressionNode> LiteralNodesList()
		{
			if (TokenValidations.TokenIsComma(_currentToken))
			{
				GoToNextToken();
				var literal = Literal();
				var literalList = LiteralNodesList();
				literalList.Insert(0, literal);
				return literalList;
			}
			Epsilon();
			return new List<ExpressionNode>();
		}

		

		#endregion
	}

	public class InitializedStructInstanceNode : StructInstanceNode
	{
		public List<ExpressionNode> LiteralNodes { get; set; }//2+5 solo literales
	}

	public class StructInstanceNode : ExpressionNode
	{
		public IdentifierNode IdentifierNode { get; set; }
	    public override BaseType ValidateSemantic()
	    {
	        return IdentifierNode.ValidateSemantic();
	    }

	    public override Value Interpret()
	    {
	        return IdentifierNode.Interpret();
	    }
	}
}