﻿using System.Linq;
using CPlusPlusCompiler.Logic.LexerComponents.Dictionary;

namespace CPlusPlusCompiler.Logic.LexerComponents
{
    public partial class Lexer
    {
        private Token TokenStartingWithPeriod(string lexeme, char currentCharacter)
        {
            lexeme += currentCharacter;
            MovePointer(1);
            var currentSymbol = GetCurrentCharacter();
            while (char.IsDigit(currentSymbol) || currentSymbol.Equals('e'))
            {
                lexeme += currentSymbol;
                MovePointer(1);
                if (currentSymbol.Equals('f') && char.IsDigit(lexeme[1]))
                    return GetToken(lexeme, TokenTypes.LITERAL_FLOAT);
                currentSymbol = GetCurrentCharacter();
            }
            if (lexeme.Length > 1)
                if (char.IsDigit(lexeme[1]))
                    return GetToken(lexeme, TokenTypes.LITERAL_FLOAT);
            return GetToken(lexeme, TokenTypes.PERIOD);
        }

        private Token TokenStartingWithLetterOrUnderscore(string lexeme, char currentCharacter)
        {
            lexeme += currentCharacter;
            MovePointer(1);
            return GetId(lexeme);
        }

        private Token TokenStartingWithSingleQuote(string lexeme, char currentCharacter)
        {
            lexeme += currentCharacter;
            MovePointer(1);
            var nextSymbol = GetCurrentCharacter();
            if (nextSymbol.Equals('\\'))
            {
                lexeme += nextSymbol;
                MovePointer(1);
                var escapeCharacter = GetCurrentCharacter();
                if (_escapeValues.Contains(escapeCharacter))
                {
                    lexeme += escapeCharacter;
                    MovePointer(1);
                    var closeChar = GetCurrentCharacter();
                    if (closeChar.Equals('\''))
                    {
                        lexeme += closeChar;
                        MovePointer(1);
                        return GetToken(lexeme, TokenTypes.LITERAL_CHAR);
                    }
                    throw new LexerException("Missing terminating \' character");
                }
                throw new LexerException("Unknown scape sequence");
            }
            lexeme += nextSymbol;
            MovePointer(1);
            var closeCharacter = GetCurrentCharacter();
            if (closeCharacter.Equals('\''))
            {
                lexeme += closeCharacter;
                MovePointer(1);
                return GetToken(lexeme, TokenTypes.LITERAL_CHAR);
            }
            throw new LexerException("Missing terminating \' character");
        }

        private Token TokenStartingWithDoubleQuote(string lexeme, char currentCharacter)
        {
            lexeme += currentCharacter;
            while (true)
            {
                MovePointer(1);
                var nextCharacter = GetCurrentCharacter();
                if (nextCharacter == '\\')
                {
                    lexeme += nextCharacter;
                    MovePointer(1);
                    var escapeValue = GetCurrentCharacter();
                    if (_escapeValues.Contains(escapeValue))
                        lexeme += escapeValue;
                    else
                        throw new LexerException("Unknown scape sequence");
                }
                else if (nextCharacter.Equals('"'))
                {
                    lexeme += nextCharacter;
                    MovePointer(1);
                    return GetToken(lexeme, TokenTypes.LITERAL_STRING);
                }
                else if (CharacterIsEndOfFile(nextCharacter) || CharacterIsNewLine(nextCharacter))
                {
                    throw new LexerException("Missing terminating \" character");
                }
                else
                {
                    lexeme += nextCharacter;
                }
            }
        }

        private Token TokenStartingWithDigit(char currentCharacter, string lexeme)
        {
            if (currentCharacter.Equals('0'))
            {
                lexeme += currentCharacter;
                MovePointer(1);
                return GetNonDecimal(lexeme);
            }
            lexeme += currentCharacter;
            MovePointer(1);
            var nextCharacter = GetCurrentCharacter();
            while (true)
                if (nextCharacter.Equals('.') || nextCharacter.Equals('e') || nextCharacter.Equals('-') ||
                    char.IsDigit(nextCharacter))
                {
                    lexeme += nextCharacter;
                    MovePointer(1);
                    nextCharacter = GetCurrentCharacter();
                }
                else if (lexeme.Contains('.') || lexeme.Contains('e') || lexeme.Contains('-'))
                {
                    return GetToken(lexeme, TokenTypes.LITERAL_FLOAT);
                }
                else
                {
                    return GetToken(lexeme, TokenTypes.Digit);
                }
        }

        private Token TokenStartingWithSymbol(char currentCharacter, string lexeme)
        {
            if (Symbols.CanBeTripleCharacterOperator(currentCharacter))
                if (_sourceCode.Length > _currentPointer + 2)
                {
                    var secondCharacter = _sourceCode[_currentPointer + 1];
                    var thirdCharacter = _sourceCode[_currentPointer + 2];
                    var posibleThreeCharacterOperator = "" + currentCharacter;
                    posibleThreeCharacterOperator += secondCharacter;
                    posibleThreeCharacterOperator += thirdCharacter;

                    if (_tripleSymbols.ContainsKey(posibleThreeCharacterOperator))
                    {
                        lexeme = posibleThreeCharacterOperator;
                        MovePointer(lexeme.Length);
                        return GetToken(lexeme, _tripleSymbols[lexeme]);
                    }
                }
            if (Symbols.CanBeDoubleCharacterOperator(currentCharacter))
                if (_sourceCode.Length > _currentPointer + 1)
                {
                    var secondCharacter = _sourceCode[_currentPointer + 1];
                    var posibleDoubleCharacterOperator = "" + currentCharacter;
                    posibleDoubleCharacterOperator += secondCharacter;

                    if (_doubleSymbols.ContainsKey(posibleDoubleCharacterOperator))
                    {
                        lexeme = posibleDoubleCharacterOperator;
                        MovePointer(lexeme.Length);
                        return GetToken(lexeme, _doubleSymbols[lexeme]);
                    }
                }
            lexeme += currentCharacter;
            MovePointer(lexeme.Length);
            return GetToken(lexeme, _simpleSymbols[lexeme]);
        }

        private Token TokenStartingWithSlash(string lexeme, char currentCharacter)
        {
            lexeme += currentCharacter;
            MovePointer(1);
			if (_sourceCode.Length - 1 >= _currentPointer)
			{
				if (_sourceCode[_currentPointer] == '/')
				{
					lexeme += _sourceCode[_currentPointer];
					MovePointer(1);
					while (true)
					{
						var currentSymbol = GetCurrentCharacter();
						if ((currentSymbol == '\n') || (currentSymbol == '\0'))
							return GetToken(lexeme, TokenTypes.COMMENT);
						lexeme += currentSymbol;
						MovePointer(1);
					}
				}
				if (_sourceCode[_currentPointer] == '*')
				{
					lexeme += _sourceCode[_currentPointer];
					MovePointer(1);
					while (true)
					{
						var currentSymbol = GetCurrentCharacter();
						if (currentSymbol == '*')
						{
							lexeme += currentSymbol;
							MovePointer(1);
							var nextSymbol = GetCurrentCharacter();
							if (nextSymbol == '/')
							{
								lexeme += nextSymbol;
								MovePointer(1);
								return GetToken(lexeme, TokenTypes.COMMENT_BLOCK);
							}
						}
						if (currentSymbol == '\0')
							throw new LexerException("No cerro el comentario de bloque exception");
						lexeme += currentSymbol;
						MovePointer(1);
					}
				}
				else if (_sourceCode[_currentPointer] == '=')
				{
					lexeme += _sourceCode[_currentPointer];
					MovePointer(1);
					return GetToken(lexeme, TokenTypes.ASSIGNMENT_DIVIDE_AND_ASSIGN);
				}
			}
            return GetToken(lexeme, TokenTypes.ARITHMETIC_DIVISION);
        }
    }
}