﻿using System.Linq;

namespace CPlusPlusCompiler.Logic.LexerComponents
{
    public partial class Lexer
    {
        private Token GetNonDecimal(string lexeme)
        {
            var tmp1 = GetCurrentCharacter();
            if (tmp1.ToString().ToLower().Equals("x"))
            {
                lexeme += tmp1;
                MovePointer(1);
                return GetHexadecimalValue(lexeme);
            }
            if (_octals.Contains(tmp1))
                return GetOctalValue(lexeme);
            if (tmp1.Equals('.'))
            {
                lexeme += tmp1;
                MovePointer(1);
                var nextCharacter = GetCurrentCharacter();
                while (true)
                    if (nextCharacter.Equals('e') || nextCharacter.Equals('-') || char.IsDigit(nextCharacter))
                    {
                        lexeme += nextCharacter;
                        MovePointer(1);
                        nextCharacter = GetCurrentCharacter();
                    }
                    else if (lexeme.Contains('.') || lexeme.Contains('e') || lexeme.Contains('-'))
                    {
                        return GetToken(lexeme, TokenTypes.LITERAL_FLOAT);
                    }
                    else
                    {
                        return GetToken(lexeme, TokenTypes.Digit);
                    }
            }
            return GetDecimal(lexeme);
        }

        private Token GetOctalValue(string lexeme)
        {
            var tmp1 = GetCurrentCharacter();
            while (_octals.Contains(tmp1))
            {
                lexeme += tmp1;
                MovePointer(1);
                tmp1 = GetCurrentCharacter();
            }
            return GetToken(lexeme, TokenTypes.OCTAL);
        }

        private Token GetHexadecimalValue(string lexeme)
        {
            var tmp1 = GetCurrentCharacter();
            while (_hexadecimals.Contains(tmp1))
            {
                lexeme += tmp1;
                MovePointer(1);
                tmp1 = GetCurrentCharacter();
            }
            return GetToken(lexeme, TokenTypes.HEX);
        }

        private Token GetDecimal(string lexeme)
        {
            var tmp1 = GetCurrentCharacter();
            while (char.IsDigit(tmp1))
            {
                lexeme += tmp1;
                MovePointer(1);
                tmp1 = GetCurrentCharacter();
            }
            return GetToken(lexeme, TokenTypes.Digit);
        }

        private Token GetId(string lexeme)
        {
            var tmp1 = GetCurrentCharacter();
            while (char.IsLetterOrDigit(tmp1) || tmp1.Equals('_'))
            {
                lexeme += tmp1;
                MovePointer(1);
                tmp1 = GetCurrentCharacter();
            }
            return GetToken(lexeme, _reserveWords.ContainsKey(lexeme) ? _reserveWords[lexeme] : TokenTypes.ID);
        }

        private Token GetHashWord(string lexeme)
        {
            var tmp1 = GetCurrentCharacter();
            while (char.IsLetter(tmp1))
            {
                lexeme += tmp1;
                MovePointer(1);
                tmp1 = GetCurrentCharacter();
            }
            return GetToken(lexeme, _hashWords.ContainsKey(lexeme) ? _hashWords[lexeme] : TokenTypes.ID);
        }
    }
}