﻿using System;

namespace CPlusPlusCompiler.Logic.LexerComponents
{
    public partial class Lexer
    {
        public class LexerException : Exception
        {
            public LexerException(string errorMessage) : base(errorMessage)
            {
            }
        }
    }
}