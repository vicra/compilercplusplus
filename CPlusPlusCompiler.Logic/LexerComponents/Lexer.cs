﻿using System;
using System.Collections.Generic;
using CPlusPlusCompiler.Logic.LexerComponents.Dictionary;

namespace CPlusPlusCompiler.Logic.LexerComponents
{
    public partial class Lexer
    {
        private readonly Dictionary<string, TokenTypes> _doubleSymbols;
        private readonly char[] _escapeValues = {'0', 'n', 't', 'v', 'b', 'r', 'f', 'a', '\\', '\'', '\"', '?'};
        private readonly Dictionary<string, TokenTypes> _hashWords;

        private readonly char[] _hexadecimals =
        {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F', 'a', 'b', 'c', 'd', 'e', 'f'
        };

        private readonly char[] _octals = {'0', '1', '2', '3', '4', '5', '6', '7'};

        private readonly Dictionary<string, TokenTypes> _reserveWords;
        private readonly Dictionary<string, TokenTypes> _simpleSymbols;
        private readonly Dictionary<string, TokenTypes> _tripleSymbols;
        private int _column;
        private int _currentPointer;
        private LexerModes _mode = LexerModes.HTML;
        private int _row = 1;

        private readonly string _sourceCode;

        public Lexer(string sourceCode)
        {
            _sourceCode = sourceCode;
            _currentPointer = 0;
            _hashWords = HashWords.GetHashWords();
            _reserveWords = ReserveWords.GetReserveWords();
            _simpleSymbols = Symbols.GetSimpleSymbols();
            _doubleSymbols = Symbols.GetDoubleSymbols();
            _tripleSymbols = Symbols.GetTripleSymbols();
        }

        public Token GetNextToken()
        {
            var lexeme = "";
            var currentCharacter = GetCurrentCharacter();
            if (_mode == LexerModes.HTML)
                while (true)
                {
                    //while (CharacterIsNewLine(currentCharacter))
                    //{
                    //    _row++;
                    //    _column = -1;
                    //    MovePointer(1);
                    //    currentCharacter = GetCurrentCharacter();
                    //}
                    //while (char.IsWhiteSpace(currentCharacter))
                    //{
                    //    MovePointer(1);
                    //    currentCharacter = GetCurrentCharacter();
                    //}
                    if (CharacterIsNewLine(currentCharacter))
                        _row++;
                    if (CharacterIsEndOfFile(currentCharacter))
                        return GetToken(lexeme, TokenTypes.EOF);
                    if (currentCharacter == '<')
                    {
                        var nextCharacter = _sourceCode[_currentPointer + 1];
                        if (nextCharacter == '%')
                        {
                            MovePointer(2);
                            _mode = LexerModes.C;
                            return GetHtmlToken(lexeme, TokenTypes.HTML_CONTENT);
                        }
                        lexeme += currentCharacter;
                        MovePointer(1);
                        currentCharacter = GetCurrentCharacter();
                    }
                    else if (CharacterIsEndOfFile(currentCharacter))
                    {
                        MovePointer(1);
                        return GetHtmlToken(lexeme, TokenTypes.HTML_CONTENT);
                    }
                    else
                    {
                        lexeme += currentCharacter;
                        MovePointer(1);
                        currentCharacter = GetCurrentCharacter();
                    }
                }
            if (_mode == LexerModes.C)
            {
                if (currentCharacter == '%')
                {
                    var nextCharacter = _sourceCode[_currentPointer + 1];
                    if (nextCharacter == '>')
                    {
                        MovePointer(2);
                        currentCharacter = GetCurrentCharacter();
                        _mode = LexerModes.HTML;
                    }
                }
                while (CharacterIsNewLine(currentCharacter))
                {
                    _row++;
                    _column = -1;
                    MovePointer(1);
                    currentCharacter = GetCurrentCharacter();
                }
                while (char.IsWhiteSpace(currentCharacter))
                {
                    MovePointer(1);
                    currentCharacter = GetCurrentCharacter();
                    while (CharacterIsNewLine(currentCharacter))
                    {
                        _row++;
                        _column = -1;
                        MovePointer(1);
                        currentCharacter = GetCurrentCharacter();
                    }
                }
                if (currentCharacter == '%')
                {
                    var nextCharacter = _sourceCode[_currentPointer + 1];
                    if (nextCharacter == '>')
                    {
                        MovePointer(2);
                        currentCharacter = GetCurrentCharacter();
                        _mode = LexerModes.HTML;

                        while (true)
                        {
                            //while (CharacterIsNewLine(currentCharacter))
                            //{
                            //    _row++;
                            //    _column = -1;
                            //    MovePointer(1);
                            //    currentCharacter = GetCurrentCharacter();
                            //}
                            //while (char.IsWhiteSpace(currentCharacter))
                            //{
                            //    MovePointer(1);
                            //    currentCharacter = GetCurrentCharacter();
                            //}
                            if (CharacterIsNewLine(currentCharacter))
                                _row++;
                            if (currentCharacter == '<')
                            {
                                var nextCharacter2 = _sourceCode[_currentPointer + 1];
                                if (nextCharacter2 == '%')
                                {
                                    MovePointer(2);
                                    _mode = LexerModes.C;
                                    return GetHtmlToken(lexeme, TokenTypes.HTML_CONTENT);
                                }
                                lexeme += currentCharacter;
                                MovePointer(1);
                                currentCharacter = GetCurrentCharacter();
                            }
                            else if (currentCharacter == '\0')
                            {
                                MovePointer(1);
                                return GetHtmlToken(lexeme, TokenTypes.HTML_CONTENT);
                            }
                            else
                            {
                                lexeme += currentCharacter;
                                MovePointer(1);
                                currentCharacter = GetCurrentCharacter();
                            }
                        }
                    }
                }
                if (CharacterIsEndOfFile(currentCharacter))
                    return GetToken(lexeme, TokenTypes.EOF);
                if (currentCharacter.Equals('/'))
                    return TokenStartingWithSlash(lexeme, currentCharacter);
                if (currentCharacter.Equals('.'))
                    return TokenStartingWithPeriod(lexeme, currentCharacter);
                if (Symbols.IsSymbol(currentCharacter))
                    return TokenStartingWithSymbol(currentCharacter, lexeme);
                if (char.IsLetter(currentCharacter) || currentCharacter.Equals('_'))
                {
                    return TokenStartingWithLetterOrUnderscore(lexeme, currentCharacter);
                }
                if (currentCharacter.Equals('#'))
                {
                    lexeme += currentCharacter;
                    if (_sourceCode.Length > _currentPointer + 1)
                    {
                        var nextCharacter = _sourceCode[_currentPointer + 1];
                        if (char.IsDigit(nextCharacter))
                        {
                            if (_sourceCode.Length > _currentPointer + 10)
                            {
                                var digit2 = _sourceCode[_currentPointer + 2];
                                var dash = _sourceCode[_currentPointer + 3];
                                var digit3 = _sourceCode[_currentPointer + 4];
                                var digit4 = _sourceCode[_currentPointer + 5];
                                var dash2 = _sourceCode[_currentPointer + 6];
                                var digit5 = _sourceCode[_currentPointer + 7];
                                var digit6 = _sourceCode[_currentPointer + 8];
                                var digit7 = _sourceCode[_currentPointer + 9];
                                var digit8 = _sourceCode[_currentPointer + 10];
                                var hash = _sourceCode[_currentPointer + 11];
                                if (char.IsDigit(nextCharacter) && char.IsDigit(digit2) &&
                                    char.IsDigit(digit3) && char.IsDigit(digit4) &&
                                    char.IsDigit(digit5) && char.IsDigit(digit6) &&
                                    char.IsDigit(digit7) && char.IsDigit(digit8) &&
                                    dash.Equals('-') && dash2.Equals('-') &&
                                    hash.Equals('#'))
                                {
                                    lexeme += nextCharacter;
                                    lexeme += digit2;
                                    lexeme += dash;
                                    lexeme += digit3;
                                    lexeme += digit4;
                                    lexeme += dash2;
                                    lexeme += digit5;
                                    lexeme += digit6;
                                    lexeme += digit7;
                                    lexeme += digit8;
                                    lexeme += hash;
                                    MovePointer(lexeme.Length);
                                    return GetToken(lexeme, TokenTypes.LITERAL_DATE);
                                }
                                throw new LexerException("Did not followed correct date format");
                            }
                            throw new LexerException("Not enough characters for date format");
                        }
                        if (char.IsLetter(nextCharacter))
                        {
                            lexeme += nextCharacter;
                            MovePointer(lexeme.Length);
                            return GetHashWord(lexeme);
                        }
                        throw new LexerException("Wrong pound(#) usage");
                    }
                }
                else if (char.IsDigit(currentCharacter))
                {
                    return TokenStartingWithDigit(currentCharacter, lexeme);
                }
                else if (currentCharacter.Equals('"'))
                {
                    return TokenStartingWithDoubleQuote(lexeme, currentCharacter);
                }
                else if (currentCharacter.Equals('\''))
                {
                    return TokenStartingWithSingleQuote(lexeme, currentCharacter);
                }
            }
            return null;
        }

        private Token GetToken(string lexeme, TokenTypes type)
        {
            return new Token
            {
                Type = type,
                Lexeme = lexeme,
                Row = _row,
                Column = _column - lexeme.Length
            };
        }

        private Token GetHtmlToken(string lexeme, TokenTypes type)
        {
            return new Token
            {
                Type = type,
                Lexeme = lexeme,
                Row = _row,
                Column = 0
            };
        }

        private static bool CharacterIsEndOfFile(char currentCharacter)
        {
            return currentCharacter == '\0';
        }

        private void MovePointer(int length)
        {
            _currentPointer += length;
            _column += length;
        }

        private static bool CharacterIsNewLine(char tmp)
        {
            return tmp == '\n';
        }

        private char GetCurrentCharacter()
        {
            if (_currentPointer < _sourceCode.Length)
                return _sourceCode[_currentPointer];
            return '\0';
        }

        public List<Token> GetAllTokens()
        {
            var returnTokens = new List<Token>();
            var currentToken = GetNextToken();
            returnTokens.Add(currentToken);
            while (currentToken.Type != TokenTypes.EOF)
            {
                currentToken = GetNextToken();
                if ((currentToken.Type != TokenTypes.COMMENT_BLOCK) && (currentToken.Type != TokenTypes.COMMENT))
                    returnTokens.Add(currentToken);
            }
            return returnTokens;
        }
    }
}