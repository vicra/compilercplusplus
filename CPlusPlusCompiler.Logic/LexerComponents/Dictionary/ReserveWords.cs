﻿using System.Collections.Generic;

namespace CPlusPlusCompiler.Logic.LexerComponents.Dictionary
{
    public class ReserveWords
    {
        public static Dictionary<string, TokenTypes> GetReserveWords()
        {
            return new Dictionary<string, TokenTypes>
            {
                {"bool", TokenTypes.TYPE_BOOLEAN},
                {"char", TokenTypes.TYPE_CHAR},
                {"date", TokenTypes.TYPE_DATE},
                {"enum", TokenTypes.TYPE_ENUM},
                {"float", TokenTypes.TYPE_FLOAT},
                {"int", TokenTypes.TYPE_INT},
                {"string", TokenTypes.TYPE_STRING},
                {"struct", TokenTypes.TYPE_STRUCT},
                {"void", TokenTypes.TYPE_VOID},
                {"break", TokenTypes.RESERVED_BREAK},
                {"case", TokenTypes.RESERVED_CASE},
                {"const", TokenTypes.RESERVED_CONST},
                {"continue", TokenTypes.RESERVED_CONTINUE},
                {"do", TokenTypes.RESERVED_DO},
                {"double", TokenTypes.RESERVED_DOUBLE},
                {"default", TokenTypes.RESERVED_DEFAULT},
                {"else", TokenTypes.RESERVED_ELSE},
                {"false", TokenTypes.RESERVED_FALSE},
                {"for", TokenTypes.RESERVED_FOR},
                {"foreach", TokenTypes.RESERVED_FOREACH},
                {"if", TokenTypes.RESERVED_IF},
                {"return", TokenTypes.RESERVED_RETURN},
                {"switch", TokenTypes.RESERVED_SWITCH},
                {"true", TokenTypes.RESERVED_TRUE},
                {"while", TokenTypes.RESERVED_WHILE}
            };
        }
    }
}