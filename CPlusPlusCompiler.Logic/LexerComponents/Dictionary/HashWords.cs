﻿using System.Collections.Generic;

namespace CPlusPlusCompiler.Logic.LexerComponents.Dictionary
{
    public class HashWords
    {
        public static Dictionary<string, TokenTypes> GetHashWords()
        {
            return new Dictionary<string, TokenTypes>
            {
                {"#include", TokenTypes.HASH_INCLUDE}
            };
        }
    }
}