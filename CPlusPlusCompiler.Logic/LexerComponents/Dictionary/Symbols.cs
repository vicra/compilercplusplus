﻿using System.Collections.Generic;

namespace CPlusPlusCompiler.Logic.LexerComponents.Dictionary
{
    public class Symbols
    {
        private static readonly List<char> SymboList = new List<char>
        {
            '+',
            '-',
            '*',
            '/',
            '%',
            '<',
            '>',
            '!',
            '&',
            '|',
            '^',
            '=',
            '(',
            ')',
            '{',
            '}',
            '[',
            ']',
            ';',
            '.',
            '~',
            ',',
            ':'
        };

        public static Dictionary<string, TokenTypes> GetSimpleSymbols()
        {
            return new Dictionary<string, TokenTypes>
            {
                {"+", TokenTypes.ARITHMETIC_ADDITION},
                {"-", TokenTypes.ARITHMETIC_SUBSTRACTION},
                {"*", TokenTypes.ARITHMETIC_MULTIPLICATION},
                {"/", TokenTypes.ARITHMETIC_DIVISION},
                {"%", TokenTypes.ARITHMETIC_MODULUS},
                {"<", TokenTypes.RELATIONAL_LESS_THAN},
                {">", TokenTypes.RELATIONAL_GREATER_THAN},
                {"!", TokenTypes.LOGICAL_NOT},
                {"&", TokenTypes.BITWISE_AND},
                {"|", TokenTypes.BITWISE_OR},
                {"^", TokenTypes.BITWISE_XOR},
                {"=", TokenTypes.ASSIGNMENT_EQUAL},
                {"(", TokenTypes.PARENTHESES_LEFT},
                {")", TokenTypes.PARENTHESES_RIGHT},
                {"{", TokenTypes.BRACE_LEFT},
                {"}", TokenTypes.BRACE_RIGHT},
                {"[", TokenTypes.BRACKET_LEFT},
                {"]", TokenTypes.BRACKET_RIGHT},
                {"'", TokenTypes.APOSTROPHE},
                {";", TokenTypes.END_STATEMENT},
                {".", TokenTypes.PERIOD},
                {"~", TokenTypes.COMPLEMENT},
                {",", TokenTypes.COMMA},
                {":", TokenTypes.COLON},
                {"?", TokenTypes.QUESTION_MARK}
            };
        }

        public static Dictionary<string, TokenTypes> GetDoubleSymbols()
        {
            return new Dictionary<string, TokenTypes>
            {
                {"++", TokenTypes.ARITHMETIC_INCREMENT},
                {"--", TokenTypes.ARITHMETIC_DECREMENT},
                {"==", TokenTypes.RELATIONAL_EQUALS},
                {"!=", TokenTypes.RELATIONAL_NOT_EQUALS},
                {">=", TokenTypes.RELATIONAL_GREATER_THAN_OR_EQUAL},
                {"<=", TokenTypes.RELATIONAL_LESS_THAN_OR_EQUAL},
                {"&&", TokenTypes.LOGICAL_AND},
                {"||", TokenTypes.LOGICAL_OR},
                {"<<", TokenTypes.BITWISE_LEFT_SHIFT},
                {">>", TokenTypes.BITWISE_RIGHT_SHIFT},
                {"+=", TokenTypes.ASSIGNMENT_ADD_AND_ASSIGN},
                {"-=", TokenTypes.ASSIGNMENT_SUBSTRACT_AND_ASSIGN},
                {"*=", TokenTypes.ASSIGNMENT_MULTIPLY_AND_ASSIGN},
                {"/=", TokenTypes.ASSIGNMENT_DIVIDE_AND_ASSIGN},
                {"%=", TokenTypes.ASSIGNMENT_MODULUS_AND_ASSIGN},
                {"&=", TokenTypes.ASSIGNMENT_BITWISE_AND_ASSIGN},
                {"^=", TokenTypes.ASSIGNMENT_BITWISE_EXCLUSIVE_OR_AND_ASSIGN},
                {"|=", TokenTypes.ASSIGNMENT_BITWISE_INCLUSIVE_OR_AND_ASSIGN},
                {"->", TokenTypes.ARROW}
            };
        }

        public static Dictionary<string, TokenTypes> GetTripleSymbols()
        {
            return new Dictionary<string, TokenTypes>
            {
                {"<<=", TokenTypes.ASSIGNMENT_LEFT_SHIFT_AND_ASSIGN},
                {">>=", TokenTypes.ASSIGNMENT_RIGHT_SHIFT_AND_ASSIGN}
            };
        }

        private static List<char> GetFirstSymbolsOfOperatorsThatCanHaveThreeSymbols()
        {
            return new List<char> {'>', '<'};
        }

        private static List<char> GetFirstSymbolsOfOperatorsThatCanHaveTwoSymbols()
        {
            return new List<char> {'+', '-', '=', '!', '>', '<', '&', '|', '*', '/', '%', '^'};
        }

        public static bool IsSymbol(char character)
        {
            return SymboList.Contains(character);
        }

        public static bool CanBeDoubleCharacterOperator(char character)
        {
            return GetFirstSymbolsOfOperatorsThatCanHaveTwoSymbols().Contains(character);
        }

        public static bool CanBeTripleCharacterOperator(char character)
        {
            return GetFirstSymbolsOfOperatorsThatCanHaveThreeSymbols().Contains(character);
        }
    }
}