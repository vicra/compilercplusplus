﻿using CPlusPlusCompiler.Logic.InterpretComponents;

namespace CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes
{
    public class ArrayType : BaseType
    {
        public override string ToString()
        {
            return "Array";
        }

        public override Value GetDefaultValue()
        {
            return new ArrayValue(){};
        }
    }
}