﻿using CPlusPlusCompiler.Logic.InterpretComponents;

namespace CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes
{
    public class DateType : BaseType
    {
       

        public override string ToString()
        {
            return "date";
        }

        public override Value GetDefaultValue()
        {
            return new DateValue()
            {
                Day = 1,
                Month = 1,
                Year = 1900
            };
        }
    }
}