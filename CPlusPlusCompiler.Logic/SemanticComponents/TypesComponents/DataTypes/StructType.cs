﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;

namespace CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes
{
    public class StructType : BaseType
    {
        public override string ToString()
        {
            return "struct";
        }

        public override Value GetDefaultValue()
        {
            return new StructValue();
        }
    }
}