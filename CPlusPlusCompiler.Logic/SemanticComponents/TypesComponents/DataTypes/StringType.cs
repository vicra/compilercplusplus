﻿using CPlusPlusCompiler.Logic.InterpretComponents;

namespace CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes
{
    public class StringType : BaseType
    {
       
        public override string ToString()
        {
            return "string";
        }

        public override Value GetDefaultValue()
        {
            return new StringValue()
            {
                
            };
        }
    }
}