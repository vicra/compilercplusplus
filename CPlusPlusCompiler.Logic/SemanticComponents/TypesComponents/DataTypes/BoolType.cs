﻿using CPlusPlusCompiler.Logic.InterpretComponents;

namespace CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes
{
    public class BoolType : BaseType
    {

        public override string ToString()
        {
            return "bool";
        }

        public override Value GetDefaultValue()
        {
            return new BoolValue()
            {
                Valor = false
            };
        }
    }
}