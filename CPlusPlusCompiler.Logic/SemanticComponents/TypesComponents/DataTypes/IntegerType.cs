﻿using CPlusPlusCompiler.Logic.InterpretComponents;

namespace CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes
{
    public class IntegerType : BaseType
    {
       
        public override string ToString()
        {
            return "int";
        }

        public override Value GetDefaultValue()
        {
            return new IntValue()
            {
                Valor = 0
            };
        }
    }
}