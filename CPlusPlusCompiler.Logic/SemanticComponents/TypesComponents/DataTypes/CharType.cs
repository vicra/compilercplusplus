﻿using CPlusPlusCompiler.Logic.InterpretComponents;

namespace CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes
{
    public class CharType : BaseType
    { 

        public override string ToString()
        {
            return "char";
        }

        public override Value GetDefaultValue()
        {
            return new CharValue()
            {
                Valor = '\0'
            };
        }
    }
}