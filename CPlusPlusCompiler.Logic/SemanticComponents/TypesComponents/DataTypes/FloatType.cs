﻿using CPlusPlusCompiler.Logic.InterpretComponents;

namespace CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes
{
    public class FloatType : BaseType
    {
        

        public override string ToString()
        {
            return "float";
        }

        public override Value GetDefaultValue()
        {
            return new FloatValue()
            {
                Valor = 0.0f
            };
        }
    }
}