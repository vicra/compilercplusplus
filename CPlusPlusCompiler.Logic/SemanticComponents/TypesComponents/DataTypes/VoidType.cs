﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;

namespace CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes
{
    public class VoidType : BaseType
    {

        public override string ToString()
        {
            return "void";
        }

        public override Value GetDefaultValue()
        {
            return new VoidValue();
        }
    }
}