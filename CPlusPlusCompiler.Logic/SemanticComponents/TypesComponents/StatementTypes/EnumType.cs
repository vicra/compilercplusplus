﻿using CPlusPlusCompiler.Logic.InterpretComponents;

namespace CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.StatementTypes
{
    public class EnumType : BaseType
    {
        public string Name { get; set; }
        
        public override string ToString()
        {
            return "enum";
        }

        public override Value GetDefaultValue()
        {
            return new EnumValue()
            {
                
            };
        }
    }
}