﻿using System.Collections.Generic;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Statement;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Compound;

namespace CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.StatementTypes
{
    public class FunctionType : BaseType
    {
        public List<ParameterNode> FunctionParams = new List<ParameterNode>();
        public BaseType ReturnType { get; set; }
        public CompoundStatementNode CompoundStatementNode { get; set; }

        public override string ToString()
        {
            return "Function";
        }

        public override Value GetDefaultValue()
        {
            return new IntValue();
        }
    }
}