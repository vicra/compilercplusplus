﻿using CPlusPlusCompiler.Logic.InterpretComponents;

namespace CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents
{
    public abstract class BaseType
    {
        public abstract override string ToString();
        public abstract Value GetDefaultValue();
    }
}