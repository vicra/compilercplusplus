﻿using System;
using System.Collections.Generic;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.StatementTypes;

namespace CPlusPlusCompiler.Logic.SemanticComponents.Tables
{
    public class TypesTable
    {
        private readonly Dictionary<string, BaseType> _table;
        public readonly Dictionary<string, Tuple<string, string>> ArrayTable = new Dictionary<string, Tuple<string, string>>();

        private static TypesTable _instance;

        private TypesTable()
        {
            _table = new Dictionary<string, BaseType>
            {
                {"int", new IntegerType()},
                {"char", new CharType()},
                {"string", new StringType()},
                {"date", new DateType()},
                {"float", new FloatType()},
                {"bool", new BoolType()},
                {"enum", new EnumType()},
                {"void", new VoidType()},
                {"struct", new StructType()},
            };
        }

        public static TypesTable Instance => _instance ?? (_instance = new TypesTable());

        public void RegisterType(string name, BaseType baseType, int row)
        {
            if (_table.ContainsKey(name))
            {
                throw new SemanticException($"Type: {name} exists.", row);
            }

            _table.Add(name, baseType);
        }

        public BaseType GetType(string name, int row)
        {
            if (_table.ContainsKey(name))
            {
                return _table[name];
            }

            throw new SemanticException($"Type: {name} doesn't exists.", row);
        }

		public BaseType GetType(string name)
		{
			if (_table.ContainsKey(name))
			{
				return _table[name];
			}

			throw new SemanticException($"Type: {name} doesn't exists.", 0);
		}


        public bool Contains(string name)
        {
            return _table.ContainsKey(name);
        }
    }
}
