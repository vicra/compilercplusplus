﻿using System;
using System.Collections.Generic;
using System.Linq;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.StatementTypes;
using CPlusPlusCompiler.Logic.TreeComponents.Statement;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.VariableDeclaration;

namespace CPlusPlusCompiler.Logic.SemanticComponents.Tables
{
    public class SymbolTable
    {
        public static readonly List<SymbolTable> SymbolTables = new List<SymbolTable>();
        public readonly List<string> _constants;
        public readonly Dictionary<Tuple<string, List<ParameterNode>>, BaseType> _funciones;
        /// <summary>
        /// definicion de variables y structs(objetos o instancias)
        /// </summary>
        public readonly Dictionary<string, BaseType> _table;

        private Dictionary<string, int> _cantidadAgregadas;
        public readonly Dictionary<string, Value> _values;
        /// <summary>
        /// templates de struct
        /// </summary>
        public readonly Dictionary<string, List<VariableDeclarationStatementNode>> _structs;

        public SymbolTable()
        {
            _cantidadAgregadas = new Dictionary<string, int>();
            _structs = new Dictionary<string, List<VariableDeclarationStatementNode>>();
            _funciones = new Dictionary<Tuple<string, List<ParameterNode>>, BaseType>();
            _values = new Dictionary<string, Value>();
            _table = new Dictionary<string, BaseType>();
            _constants = new List<string>();
            DeclareVariable("print", new FunctionType(), -1);
        }

        public static SymbolTable Instance
        {
            get
            {
                if (SymbolTables.Count == 0)
                    SymbolTables.Add(new SymbolTable());

                return SymbolTables[0];
            }
        }

        public static void AddSymbolTable(SymbolTable symbolTable)
        {
            SymbolTables.Insert(0, symbolTable);
        }

        public static void RemoveSymbolTable()
        {
            SymbolTables.RemoveAt(0);
        }

        public void DeclareVariable(string name, string typeName, int row)
        {
            if (_table.ContainsKey(name))
                throw new SemanticException($"Variable: {name} already exists.", row);
            if (TypesTable.Instance.Contains(name))
                throw new SemanticException($"{name} is a Type.", row);

            _table.Add(name, TypesTable.Instance.GetType(typeName, row));
            var defaultValue = TypesTable.Instance.GetType(typeName, row).GetDefaultValue();
            _values.Add(name, defaultValue);
        }

        public void DeclareVariable(string name, BaseType type, int row)
        {
            if (type is FunctionType && (name != "print"))
            {
                //verificar parametros
                if (_table.ContainsKey(name))
                {
                    var existingFunctionType = _table[name];
                    var currentType = (FunctionType) type;
                    if (existingFunctionType is FunctionType)
                    {
                        var existingDeclaration = (FunctionType) existingFunctionType;
                        if (existingDeclaration.FunctionParams.Count != currentType.FunctionParams.Count)
                        {
                            //lo crea
                            _table.Add(name, type);
                        }
                        else
                        {
                            //verificar tipo parametros
                            var sonIguales = true;
                            for (var i = 0; i < existingDeclaration.FunctionParams.Count; i++)
                                if (existingDeclaration.FunctionParams[i].Type != currentType.FunctionParams[i].Type)
                                    sonIguales = false;
                            if (sonIguales)
                                throw new SemanticException("Declaracion de funcion ya existe", row);
                            _table.Add(name, type);
                        }
                    }
                    else
                    {
                        throw new SemanticException("Variable ya existe, no como funcion", row);
                    }
                }
                else
                {
                    //funcion no existe 
                    _table.Add(name, type);
                }
            }
            else
            {
                if (_table.ContainsKey(name))
                    throw new SemanticException($"Variable: {name} already exists.", row);
                if (TypesTable.Instance.Contains(name))
                    throw new SemanticException($"{name} is a Type.", row);

                _table.Add(name, type);
                var defaultValue = type.GetDefaultValue();
                _values.Add(name, defaultValue);
            }
        }

        public void DeclareFunction(string name, BaseType type, int row, List<ParameterNode> parameters)
        {
            for (var n = 0; n < _funciones.Count; n++)
                if (_funciones.ElementAt(n).Key.Item1.Equals(name))
                    if (_funciones.ElementAt(n).Key.Item2.Count == parameters.Count)
                    {
                        var parametrosSonIguales = true;
                        for (var i = 0; i < _funciones.ElementAt(n).Key.Item2.Count; i++)
                            if (_funciones.ElementAt(n).Key.Item2[i].Type != parameters[i].Type)
                                parametrosSonIguales = false;
                        if (!parametrosSonIguales)
                        {
                            //agregar la funcion pq los parametrso son distintos
                            _funciones.Add(new Tuple<string, List<ParameterNode>>(name, parameters), type);
                            return;
                        }
                        //tira error pq parametros son iguales
                        throw new SemanticException($"Funcion {name}, ya existe con parametros esos parametros", row);
                    }
                    else
                    {
                        //agregar la funcion pq tienen cantidad de parametros diferentes
                        _funciones.Add(new Tuple<string, List<ParameterNode>>(name, parameters), type);
                        return;
                    }
            if (_funciones.ContainsKey(new Tuple<string, List<ParameterNode>>(name, parameters)))
                throw new SemanticException($"Function: {name} with parameters {parameters} already exists.", row);
            _funciones.Add(new Tuple<string, List<ParameterNode>>(name, parameters), type);
        }

        public void DeclareStruct(string name, string type, int row, 
            List<VariableDeclarationStatementNode> variableDeclarations)
        {
            //validar que no exista ni en _table ni _structs
            if (_table.ContainsKey(name) || _structs.ContainsKey(name))
            {
                throw new SemanticException($"Variable {name} already exists" ,row);
            }
            _structs.Add(name, variableDeclarations);
            TypesTable.Instance.RegisterType(name, 
                TypesTable.Instance.GetType(type, row),row);
        }

        public BaseType GetFunction(string name, int row, List<BaseType> parameters)
        {
            foreach (var symbolTable in SymbolTables)
            {
                // iterador de las funciones
                for (int i = 0; i < symbolTable._funciones.Count; i++)
                {
                    //si tienen el mismo nombre
                    if (symbolTable._funciones.Keys.ElementAt(i).Item1.Equals(name))
                    {
                        if (symbolTable._funciones.Keys.ElementAt(i).Item2.Count == parameters.Count)
                        {
                            bool parametrosSonIguales = true;
                            //for que valida si los parametros son del mismo tipo
                            for (int j = 0; j < symbolTable._funciones.Keys.ElementAt(i).Item2.Count; j++)
                            {
                                if (symbolTable._funciones.Keys.ElementAt(i).Item2.ElementAt(j).Type != parameters[j])
                                {
                                    parametrosSonIguales = false;
                                }
                            }
                            if (!parametrosSonIguales)
                            {
                                //tira error pq no encontro
                                throw new SemanticException($"Functions: {name} doesn't exists.", row);
                            }
                            else
                            {
                                //si encontro la funcion y la devuelve
                                return symbolTable._funciones.ElementAt(i).Value;
                            }
                        }
                    }
                }
            }
            throw new SemanticException($"Functions: {name} doesn't exists.", row);
        }

        public BaseType GetVariable(string name, int row)
        {
            foreach (var symbolTable in SymbolTables)
                if (symbolTable._table.ContainsKey(name))
                    return symbolTable._table[name];
            throw new SemanticException($"Variable: {name} doesn't exists.", row);
        }

        public void AddConstant(string value)
        {
            _constants.Add(value);
        }

        public bool GetConstant(string value)
        {
            return _constants.Contains(value);
        }

        public void SetVariableValue(string name, Value value)
        {
            _values[name] = value;
        }

        public Value GetVariableValue(string name)
        {
            return _values[name];
        }

        public bool VariableExist(string name)
        {
            return _table.ContainsKey(name);
        }

        public Value GetVariableValueFunction(string name)
        {
            foreach (var symbolTable in SymbolTables)
            {
                if (symbolTable._values.ContainsKey(name))
                {
                    return symbolTable._values[name];
                }
            }
            return null;
        }
    }
}