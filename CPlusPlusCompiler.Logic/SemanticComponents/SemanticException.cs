﻿using System;

namespace CPlusPlusCompiler.Logic.SemanticComponents
{
    public class SemanticException : Exception
    {
        public SemanticException(string message, int row)
			: base(message + $"\nError in line: {row} ")
        {
            
        }
    }
}