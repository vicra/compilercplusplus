﻿namespace CPlusPlusCompiler.Logic.InterpretComponents
{
    public class CharValue : Value
    {
        public override Value Clone()
        {
            var thisValue = Valor;
            return new CharValue
            {
                Valor = thisValue
            };
        }

        public override string ToString()
        {
            return Valor.ToString();
        }
    }
}