﻿namespace CPlusPlusCompiler.Logic.InterpretComponents
{
    public abstract class Value
    {
        public dynamic Valor { get; set; }
        public abstract Value Clone();
    }
}