﻿namespace CPlusPlusCompiler.Logic.InterpretComponents
{
    public class StringValue : Value
    {
        public override Value Clone()
        {
            var thisValue = Valor;
            return new StringValue
            {
                Valor = thisValue
            };
        }

        public override string ToString()
        {
            return Valor.ToString();
        }
    }
}