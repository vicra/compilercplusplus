﻿using System;

namespace CPlusPlusCompiler.Logic.InterpretComponents
{
    public class VoidValue : Value
    {
        public override Value Clone()
        {
            return new VoidValue();
        }
    }
}