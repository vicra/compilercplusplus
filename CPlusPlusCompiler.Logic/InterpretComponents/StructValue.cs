﻿using System;
using System.Collections.Generic;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.VariableDeclaration;

namespace CPlusPlusCompiler.Logic.InterpretComponents
{
    public class StructValue : Value
    {
        public List<dynamic> Propiedades { get; set; } = new List<dynamic>();

        public override Value Clone()
        {
            return new StructValue()
            {
                Propiedades = this.Propiedades
            };
        }
    }
}