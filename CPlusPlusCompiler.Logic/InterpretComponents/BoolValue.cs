﻿namespace CPlusPlusCompiler.Logic.InterpretComponents
{
    public class BoolValue : Value
    {
        public override Value Clone()
        {
            var thisValue = Valor;
            return new BoolValue
            {
                Valor = thisValue
            };
        }

        public override string ToString()
        {
            return Valor.ToString();
        }
    }
}