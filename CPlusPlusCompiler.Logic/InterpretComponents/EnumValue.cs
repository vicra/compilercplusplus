﻿using System;

namespace CPlusPlusCompiler.Logic.InterpretComponents
{
    public class EnumValue : Value
    {
        public override Value Clone()
        {
            return new EnumValue()
            {
                Valor = this.Valor
            };
        }
    }
}