﻿namespace CPlusPlusCompiler.Logic.InterpretComponents
{
    public class IntValue : Value
    {
        public override Value Clone()
        {
            var thisValue = Valor;
            return new IntValue
            {
                Valor = thisValue
            };
        }

        public override string ToString()
        {
            return Valor.ToString();
        }
    }
}