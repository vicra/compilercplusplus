﻿namespace CPlusPlusCompiler.Logic.InterpretComponents
{
    public class FloatValue : Value
    {
        public override Value Clone()
        {
            var thisValue = Valor;
            return new FloatValue
            {
                Valor = thisValue
            };
        }

        public override string ToString()
        {
            return Valor.ToString();
        }
    }
}