﻿namespace CPlusPlusCompiler.Logic.InterpretComponents
{
    public class DateValue : Value
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }

        public override Value Clone()
        {
            var thisValue = Valor;
            var thisDay = Day;
            var thisMonth = Month;
            var thisYear = Year;
            return new DateValue
            {
                Valor = thisValue,
                Day = thisDay,
                Month = thisMonth,
                Year = thisYear
            };
        }

        public override string ToString()
        {
            return Valor.ToString();
        }
    }
}