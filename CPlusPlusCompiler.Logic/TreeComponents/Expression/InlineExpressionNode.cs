﻿using System;
using System.Collections.Generic;
using System.Linq;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression
{
    public class InlineExpressionNode : ExpressionNode
    {
        public List<ExpressionNode> AssignmentExpressions = new List<ExpressionNode>();
        public override BaseType ValidateSemantic()
        {
            var type = AssignmentExpressions.First().ValidateSemantic();
            foreach (var expression in AssignmentExpressions)
            {
                type = expression.ValidateSemantic();
            }
            return type;
        }

        public override Value Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
