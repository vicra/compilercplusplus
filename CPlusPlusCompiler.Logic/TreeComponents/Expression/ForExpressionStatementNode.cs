﻿using System;
using System.Linq.Expressions;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression
{
    public class ForExpressionStatementNode : ExpressionNode
	{
		public ExpressionNode ExpressionA { get; set; }
		public BinaryOperatorNode ExpressionB { get; set; }
		public UnaryOperatorNode ExpressionC { get; set; }
		public IdentifierNode LeftIdentifier { get; set; }
		public IdentifierNode RightIdentifier { get; set; }

		public override BaseType ValidateSemantic()
		{
		    ExpressionA?.ValidateSemantic();
            ExpressionB?.ValidateSemantic();
            ExpressionC?.ValidateSemantic();
            LeftIdentifier?.ValidateSemantic();
            RightIdentifier?.ValidateSemantic();
		    return new BoolType();
		}

	    public override Value Interpret()
	    {
	        return new BoolValue();
	    }
	}
}
