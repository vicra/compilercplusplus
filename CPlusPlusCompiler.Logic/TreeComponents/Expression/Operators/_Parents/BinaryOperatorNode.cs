﻿using System;
using System.Collections.Generic;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents
{
    public abstract class BinaryOperatorNode : ExpressionNode
    {
        public Dictionary<Tuple<BaseType, BaseType>, BaseType> RulesDictionary = new Dictionary<Tuple<BaseType, BaseType>, BaseType>();
        public ExpressionNode LeftNode { get; set; }
        public ExpressionNode RightNode { get; set; }

        public override BaseType ValidateSemantic()
        {
            var leftType = LeftNode.ValidateSemantic();
            var rightType = RightNode.ValidateSemantic();
            var key = new Tuple<BaseType, BaseType>(leftType, rightType);
            if (RulesDictionary.ContainsKey(key))
            {
                return RulesDictionary[key];
            }
            throw new SemanticException(SemanticError(leftType, rightType), Row);
        }

        public abstract string SemanticError(BaseType leftType, BaseType rightType);
    }
}