﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents
{
    public class TernaryOperatorNode : ExpressionNode
    {
        public TernaryOperatorNode(ExpressionNode condition, ExpressionNode ifTrue, ExpressionNode ifFalse)
        {
            Condition = condition;
            IfTrue = ifTrue;
            IfFalse = ifFalse;
        }

        public ExpressionNode Condition { get; set; }
        public ExpressionNode IfTrue { get; set; }
        public ExpressionNode IfFalse { get; set; }

        public override BaseType ValidateSemantic()
        {
            var conditionType = Condition.ValidateSemantic();
            if (!(conditionType is BoolType))
                throw new SemanticException($"Condition needs to be boolean", Row);
            var trueType = IfTrue.ValidateSemantic();
            var falseType = IfFalse.ValidateSemantic();
            if (trueType != falseType)
                throw new SemanticException($"{trueType} type return vales is different from {falseType} type", Row);
            return trueType;
        }

        public override Value Interpret()
        {
            throw new NotImplementedException();
        }
    }
}