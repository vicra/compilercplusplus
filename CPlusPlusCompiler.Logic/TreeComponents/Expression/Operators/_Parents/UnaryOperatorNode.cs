﻿namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents
{
    public abstract class UnaryOperatorNode : ExpressionNode
    {
        public ExpressionNode Expression { get; set; }
    }
}