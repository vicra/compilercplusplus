﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Relational
{
    public class LessThanOperatorNode : BinaryOperatorNode
    {
        public LessThanOperatorNode()
        {
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, Integer), Boolean);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Float, Float), Boolean);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Float, Integer), Boolean);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, Float), Boolean);
        }

        public override string SemanticError(BaseType leftType, BaseType rightType)
        {
            return $"Less Than is not supported between {leftType} and {rightType} types.";
        }

        public override Value Interpret()
        {
            var leftValue = LeftNode.Interpret();
            var rightValue = RightNode.Interpret();
            if (leftValue.Valor < rightValue.Valor)
            {
                return new BoolValue()
                {
                    Valor = true
                };
            }
            return new BoolValue()
            {
                Valor = false
            };
        }
    }
}