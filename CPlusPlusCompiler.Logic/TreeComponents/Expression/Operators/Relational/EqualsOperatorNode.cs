using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Relational
{
    public class EqualsOperatorNode : BinaryOperatorNode
    {
        public EqualsOperatorNode()
        {
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, Integer), Boolean);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(String, String), Boolean);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Float, Float), Boolean);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Float, Integer), Boolean);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, Float), Boolean);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Char, Char), Boolean);
        }

        public override string SemanticError(BaseType leftType, BaseType rightType)
        {
            return $"Comparison is not supported between {leftType} and {rightType} types.";
        }

        public override Value Interpret()
        {
            var leftValue = LeftNode.Interpret();
            var rightValue = RightNode.Interpret();
            if (leftValue.Valor == rightValue.Valor)
            {
                return new BoolValue()
                {
                    Valor = true
                };
            }
            return new BoolValue()
            {
                Valor = false
            };
        }
    }
}