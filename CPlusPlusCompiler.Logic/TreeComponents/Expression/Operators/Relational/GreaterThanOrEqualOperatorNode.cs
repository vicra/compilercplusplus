﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression._Parent;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Relational
{
    public class GreaterThanOrEqualOperatorNode : BinaryOperatorNode
    {
        public GreaterThanOrEqualOperatorNode()
        {
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, Integer), Boolean);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Float, Float), Boolean);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Float, Integer), Boolean);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, Float), Boolean);
        }

        public override string SemanticError(BaseType leftType, BaseType rightType)
        {
            return $"Greater Than Or Equal is not supported between {leftType} and {rightType} types.";
        }

        public override Value Interpret()
        {
            var leftValue = LeftNode.Interpret();
            var rightValue = RightNode.Interpret();
            if (leftValue.Valor >= rightValue.Valor)
            {
                return new BoolValue()
                {
                    Valor = true
                };
            }
            return new BoolValue()
            {
                Valor = false
            };
        }
    }
}