﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Arithmetic
{
    public class AdditionNode : BinaryOperatorNode
    {
        public AdditionNode()
        {
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, Integer), Integer);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(String, String), String);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(String, Integer), String);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(String, Float), String);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(String, Char), String);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(String, Boolean), String);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(String, Date), String);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, String), String);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Float, Float), Float);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Float, Integer), Float);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, Float), Float);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Char, Char), String);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Char, Integer), String);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, Char), String);
        }

        public override string SemanticError(BaseType leftType, BaseType rightType)
        {
            return $"Addition is not supported between {leftType} and {rightType} types.";
        }

        public override Value Interpret()
        {
            var left = LeftNode.Interpret();
            var right = RightNode.Interpret();
            dynamic result = left.Valor + right.Valor;
            if (left is IntValue)
            {
                if (right is IntValue) return new IntValue{ Valor = result};
                if (right is StringValue) return new StringValue { Valor = result };
                if (right is FloatValue) return new FloatValue { Valor = result };
                if (right is CharValue) return new StringValue { Valor = result };
            }
            if (left is StringValue)
            {
                if (right is IntValue) return new StringValue { Valor = result };
                if (right is StringValue) return new StringValue { Valor = result };
                if (right is FloatValue) return new StringValue { Valor = result };
                if (right is CharValue) return new StringValue { Valor = result };
                if (right is DateValue) return new StringValue { Valor = result };
                if (right is BoolValue) return new StringValue { Valor = result };
            }
            if (left is FloatValue)
            {
                if (right is FloatValue) return new FloatValue { Valor = result };
                if (right is IntValue) return new FloatValue { Valor = result };
            }
            if (left is CharValue)
            {
                if (right is CharValue) return new StringValue { Valor = result };
                if (right is IntValue) return new StringValue { Valor = result };
            }
            return null;
        }
    }
}