﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Arithmetic
{
    public class ModulusNode : BinaryOperatorNode
    {
        public ModulusNode()
        {
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, Integer), Integer);
        }

        public override string SemanticError(BaseType leftType, BaseType rightType)
        {
            return $"Modulus is not supported between {leftType} and {rightType} types.";
        }

        public override Value Interpret()
        {
            var left = LeftNode.Interpret();
            var right = RightNode.Interpret();
            dynamic result = left.Valor - right.Valor;
            if (left is IntValue)
            {
                if (right is IntValue) return new IntValue { Valor = result };
            }
            return null;
        }
    }
}