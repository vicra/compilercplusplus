﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Arithmetic
{
    public class PostfixIncrementNode : UnaryOperatorNode
    {
        public override BaseType ValidateSemantic()
        {
            var type = Expression.ValidateSemantic();
            if (!(type is IntegerType) && !(type is FloatType) && !(type is CharType))
                throw new SemanticException($"Expression: {type} can't be postfix incremented", Row);
            return type;
        }

        public override Value Interpret()
        {
            var idName = ((IdentifierNode) Expression).Name;
            var currentValue = (IntValue) Expression.Interpret();
            var newValue = new IntValue
            {
                Valor = currentValue.Valor + 1
            };
            SymbolTable.Instance.SetVariableValue(idName, newValue);
            return newValue;
        }
    }
}