﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Arithmetic
{
    public class MultiplicationNode : BinaryOperatorNode
    {
        public MultiplicationNode()
        {
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, Integer), Integer);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Integer, Float), Float);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Float, Float), Float);
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Float, Integer), Float);
        }

        public override string SemanticError(BaseType leftType, BaseType rightType)
        {
            return $"Multiplication is not supported between {leftType} and {rightType} types.";
        }

        public override Value Interpret()
        {
            var left = LeftNode.Interpret();
            var right = RightNode.Interpret();
            dynamic result = left.Valor * right.Valor;
            if (left is IntValue)
            {
                if (right is IntValue) return new IntValue { Valor = result };
                if (right is FloatValue) return new FloatValue { Valor = result };
            }
            if (left is FloatValue)
            {
                if (right is FloatValue) return new FloatValue { Valor = result };
                if (right is IntValue) return new FloatValue { Valor = result };
            }
            return null;
        }
    }
}