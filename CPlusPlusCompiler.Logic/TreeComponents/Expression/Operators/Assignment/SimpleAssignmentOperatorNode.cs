using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Assignment
{
    public class SimpleAssignmentOperatorNode : BinaryOperatorNode
    {
        public override BaseType ValidateSemantic()
        {
            var leftType = LeftNode.ValidateSemantic();
            var rightType = RightNode.ValidateSemantic();

            if (leftType != rightType)
                throw new SemanticException(SemanticError(leftType, rightType), Row);
            return leftType;
        }

        public override Value Interpret()
        {
            throw new NotImplementedException();
        }

        public override string SemanticError(BaseType leftType, BaseType rightType)
        {
            return $"Assignment is not supported between {leftType} and {rightType} types.";
        }
    }
}