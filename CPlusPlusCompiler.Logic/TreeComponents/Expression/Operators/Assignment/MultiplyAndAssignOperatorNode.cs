﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Assignment
{
    public class MultiplyAndAssignOperatorNode : BinaryOperatorNode
    {
        public MultiplyAndAssignOperatorNode(IdentifierNode parameter, ExpressionNode expressionNode)
        {
            LeftNode = parameter;
            RightNode = expressionNode;
        }

        public override BaseType ValidateSemantic()
        {
            var leftType = LeftNode.ValidateSemantic();
            var rightType = RightNode.ValidateSemantic();

            if (leftType != rightType)
                throw new SemanticException(SemanticError(leftType, rightType), Row);
            return leftType;
        }

        public override Value Interpret()
        {
            var currentValue = LeftNode.Interpret();
            var addingValue = RightNode.Interpret();
            var newValue = currentValue.Valor * addingValue.Valor;
            SymbolTable.Instance.SetVariableValue(((IdentifierNode)LeftNode).Name, newValue);
            return currentValue;
        }

        public override string SemanticError(BaseType leftType, BaseType rightType)
        {
            return $"Multiply and assign is not supported between {leftType} and {rightType} types.";
        }
    }
}