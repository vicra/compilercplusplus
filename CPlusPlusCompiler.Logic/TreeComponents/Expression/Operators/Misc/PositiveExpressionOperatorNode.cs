using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Misc
{
    public class PositiveExpressionOperatorNode : UnaryOperatorNode
    {
        public override BaseType ValidateSemantic()
        {
            return Expression.ValidateSemantic();
        }

        public override Value Interpret()
        {
            throw new System.NotImplementedException();
        }
    }
}