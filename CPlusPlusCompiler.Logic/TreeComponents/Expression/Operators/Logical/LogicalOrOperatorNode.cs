﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Logical
{
    public class LogicalOrOperatorNode : BinaryOperatorNode
    {
        public LogicalOrOperatorNode()
        {
            RulesDictionary.Add(new Tuple<BaseType, BaseType>(Boolean, Boolean), Boolean);
        }
        public override string SemanticError(BaseType leftType, BaseType rightType)
        {
            return $"And is not supported between {leftType} and {rightType} expressions.";
        }

        public override Value Interpret()
        {
            var leftValue = LeftNode.Interpret();
            var rightValue = RightNode.Interpret();
            if (leftValue.Valor || rightValue.Valor)
            {
                return new BoolValue()
                {
                    Valor = true
                };
            }
            return new BoolValue()
            {
                Valor = false
            };
        }
    }
}