﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Logical
{
    public class LogicalNotOperatorNode : _Parents.UnaryOperatorNode
    {
        public override BaseType ValidateSemantic()
        {
            var type = Expression.ValidateSemantic();
            if (!(type is BoolType))
                throw new SemanticException($"Expression: {type} can't be negated..", Row);

            return new BoolType();
        }

        public override Value Interpret()
        {
            var value = Expression.Interpret();
            return !value.Valor;
        }
    }
}