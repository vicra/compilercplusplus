﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Bitwise
{
    public class BitwiseOrOperatorNode : BinaryOperatorNode
    {

        public override string SemanticError(BaseType leftType, BaseType rightType)
        {
            return $"Unable to perform bitwise OR operation between {leftType} and {rightType} types.";
        }

        public override Value Interpret()
        {
            throw new NotImplementedException();
        }
    }
}