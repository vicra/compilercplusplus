﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression._Parent;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Bitwise
{
    public class BitwiseShiftLeftOperatorNode : BinaryOperatorNode
    {
        

        public override string SemanticError(BaseType leftType, BaseType rightType)
        {
            return $"Unable to perform bitwise LEFT SHIFT operation between {leftType} and {rightType} types.";
        }

        public override Value Interpret()
        {
            throw new NotImplementedException();
        }
    }
}