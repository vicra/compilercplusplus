﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression
{
    public abstract class ExpressionNode
    {
        public BaseType Integer = TypesTable.Instance.GetType("int");
        public BaseType String = TypesTable.Instance.GetType("string");
        public BaseType Boolean = TypesTable.Instance.GetType("bool");
        public BaseType Char = TypesTable.Instance.GetType("char");
        public BaseType Date = TypesTable.Instance.GetType("date");
        public BaseType Float = TypesTable.Instance.GetType("float");

		public int Row { get; set; }

        public abstract BaseType ValidateSemantic();
        public abstract Value Interpret();
    }
}