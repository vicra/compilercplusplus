﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression._Parent;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Literals
{
    public class CharLiteralNode : LiteralNode
    {
        public override BaseType ValidateSemantic()
        {
            return TypesTable.Instance.GetType("char");
        }

        public override Value Interpret()
        {
            return new CharValue()
            {
                Valor = Lexeme
            };
        }
    }
}