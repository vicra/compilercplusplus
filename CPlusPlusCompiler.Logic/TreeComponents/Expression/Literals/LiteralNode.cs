﻿namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Literals
{
    public abstract class LiteralNode : ExpressionNode
    {
        public string Lexeme { get; set; }
    }
}