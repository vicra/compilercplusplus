﻿using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Literals
{
    public class EnumAssignerNode
    {
        public ExpressionNode ValueAssignedNode { get; set; }

        public IdentifierNode IdentifierNode { get; set; }
    }
}