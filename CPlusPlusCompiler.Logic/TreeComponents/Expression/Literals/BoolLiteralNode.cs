using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Literals
{
    public class BoolLiteralNode : LiteralNode
    {
        public override BaseType ValidateSemantic()
        {
            return TypesTable.Instance.GetType("bool");
        }

        public override Value Interpret()
        {
            return new BoolValue()
            {
                Valor = (Lexeme == "false")?false:true
            };
        }
    }
}