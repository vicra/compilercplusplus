﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Literals
{
    public class IntLiteralNode : LiteralNode
    {
        public override BaseType ValidateSemantic()
        {
            return TypesTable.Instance.GetType("int");
        }

        public override Value Interpret()
        {
            return new IntValue
            {
                Valor = int.Parse(Lexeme)
            };
        }
    }
}