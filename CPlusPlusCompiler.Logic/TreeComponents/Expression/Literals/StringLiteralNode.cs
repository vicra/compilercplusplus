﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Literals
{
    public class StringLiteralNode : LiteralNode
    {
        public override BaseType ValidateSemantic()
        {
            return TypesTable.Instance.GetType("string");
        }

        public override Value Interpret()
        {
            var cadena = Lexeme.TrimStart('\"');
            var cadena2 = cadena.TrimEnd('\"');
            return new StringValue
            {
                Valor = cadena2
            };
        }
    }
}