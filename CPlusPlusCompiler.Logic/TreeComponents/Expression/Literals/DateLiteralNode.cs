﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Literals
{
    public class DateLiteralNode : LiteralNode
    {
        public override BaseType ValidateSemantic()
        {
            return TypesTable.Instance.GetType("date");
        }

        public override Value Interpret()
        {
            var removedHash = Lexeme.TrimEnd('#');
            var removedStart = removedHash.TrimStart('#');
            var split = removedStart.Split('-');
            return new DateValue
            {
                Day = int.Parse(split[0]),
                Month = int.Parse(split[1]),
                Year = int.Parse(split[2]),
                Valor = removedStart
            };
        }
    }
}