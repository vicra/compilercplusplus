﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Literals
{
    public class EnumeratorNode : ExpressionNode
    {
        public LiteralNode ValueLiteral { get; set; }

        public IdentifierNode IdentifierNode { get; set; }
        public override BaseType ValidateSemantic()
        {
            throw new System.NotImplementedException();
        }

        public override Value Interpret()
        {
            throw new System.NotImplementedException();
        }
    }
}