﻿using System;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression._Parent
{
    public class ExpressionStatementNode : BasicStatementNode //Statement node ?
    {
        public ExpressionNode ExpressionNode { get; set; }

		public override string Interpret()
		{
		    var statement = "";
		    if (ExpressionNode != null)
		    {
                statement = ExpressionNode.Interpret().ToString();
            }
            
		    if (ExpressionNode is PrintStatementNode)
		    {
		        return statement + "<br>";
		    }
		    return "";
		}

		public override void ValidateSemantic()
        {
			ExpressionNode.ValidateSemantic();
        }
    }
}