using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Id.Accessors
{
    public class IndexAccesorNode : AccesorNode
    {
        public ArrayType AccessorType { get; set; }
        public ExpressionNode IndexExpression { get; set; }

        public override BaseType Validate()
        {
            return IndexExpression.ValidateSemantic();
        }
    }
}