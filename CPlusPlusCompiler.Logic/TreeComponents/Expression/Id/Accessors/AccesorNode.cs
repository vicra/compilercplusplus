﻿using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Id.Accessors
{
	public abstract class AccesorNode
	{
	    public abstract BaseType Validate();
	}
}