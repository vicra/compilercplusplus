using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Id.Accessors
{

	public class PropertyAccesorNode : AccesorNode
	{
	    public IdentifierNode IdentifierNode { get; set; }

	    public override BaseType Validate()
	    {
	        return IdentifierNode.ValidateSemantic();
	    }
	}
	
}