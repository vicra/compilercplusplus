﻿using System;
using System.Collections.Generic;
using System.Linq;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id.Accessors;

namespace CPlusPlusCompiler.Logic.TreeComponents.Expression.Id
{
    public class IdentifierNode : ExpressionNode
    {
        public List<AccesorNode> Accesors = new List<AccesorNode>();
        public string Name { get; set; }
        public bool IsConst { get; set; }
        public int PointerLevel { get; set; }
        public string TypeSpecifier { get; set; }

        public override BaseType ValidateSemantic()
        {
            
            var type = SymbolTable.Instance.GetVariable(Name, Row);
            return type;
        }

        public override Value Interpret()
        {
            if (Accesors.Count > 0)
            {
                for (int i = 0; i < SymbolTable.Instance._table.Count; i++)
                {
                    //obtener el struct
                    if (SymbolTable.Instance._table.ElementAt(i).Key.Equals(Name))
                    {
                        var accessorName = ((PropertyAccesorNode)Accesors[0]).IdentifierNode.Name;
                        //obtener el objecto del struct
                        var accessor = SymbolTable.Instance._values.ElementAt(i).Value;
                        Console.Write("");
                    }
                }
            }
            return SymbolTable.Instance.GetVariableValue(Name);
        }

        public Value InterpretFunction()
        {
            return SymbolTable.Instance.GetVariableValueFunction(Name);
        }
    }
}