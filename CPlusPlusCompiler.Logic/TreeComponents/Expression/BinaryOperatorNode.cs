﻿namespace CPlusPlusCompiler.Logic.TreeComponents.Expression
{
    public abstract class BinaryOperatorNode : ExpressionNode
    {
        protected BinaryOperatorNode(ExpressionNode leftNode, ExpressionNode rightNode)
        {
            LeftNode = leftNode;
            RightNode = rightNode;
        }

        public ExpressionNode LeftNode { get; set; }
        public ExpressionNode RightNode { get; set; }
    }
}