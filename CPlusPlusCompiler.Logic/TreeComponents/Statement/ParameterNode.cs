﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SyntacticComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement
{
	public class ParameterNode : ExpressionNode
	{
	    public IdentifierNode Identifier { get; set; }
        public BaseType Type { get; set; }
        public bool ByReference { get; set; }
        public override BaseType ValidateSemantic()
	    {
            SymbolTable.Instance.DeclareVariable(Identifier.Name, Identifier.TypeSpecifier, Row);
            var type = Identifier.ValidateSemantic();
	        return type;
        }

	    public override Value Interpret()
	    {
	        return Identifier.Interpret();
	    }
	}

    public class StructParameterNode : ParameterNode
    {
        public IdentifierNode TypeSpecifier { get; set; }
    }
}