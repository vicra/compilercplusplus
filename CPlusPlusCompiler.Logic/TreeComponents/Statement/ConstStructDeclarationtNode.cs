﻿using System.Collections.Generic;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SyntacticComponents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement
{
    public class ConstStructDeclarationtNode : StructDeclarationNode
    {
        public List<InitializedStructInstanceNode> InitializedStructs { get; set; }

		public override void ValidateSemantic()
		{
			if (InitializedStructs.Count == 0)
			{
				throw new SemanticException("abstract declarator 'const<anonymous struct>' used as declaration", Row);
			}
		}
    }
}