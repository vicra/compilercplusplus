﻿using System.Collections.Generic;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.StatementTypes;
using CPlusPlusCompiler.Logic.TreeComponents.Expression;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Jump;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement
{
    public class CallFunctionNode : ExpressionNode
    {
        public SymbolTable FunctionLocalTable = new SymbolTable();
        public string FunctionName { get; set; }
        public List<ExpressionNode> Parameters { get; set; }
        public string StatementsReturn { get; set; } = "";

        public override BaseType ValidateSemantic()
        {
            var parameterListBaseTypes = new List<BaseType>();
            foreach (var parameterNode in Parameters)
                parameterListBaseTypes.Add(parameterNode.ValidateSemantic());
            var functionStored = SymbolTable.Instance.GetFunction(FunctionName, Row, parameterListBaseTypes);
            if (!(functionStored is FunctionType))
                throw new SemanticException($"{FunctionName} is not a function.", Row);
            var functionStoredObject = (FunctionType) functionStored;
            if (functionStoredObject.FunctionParams.Count != Parameters.Count)
                throw new SemanticException($"Argument count dont match on function {FunctionName}.", Row);

            for (var i = 0; i < Parameters.Count; i++)
            {
                var paramType = Parameters[i].ValidateSemantic();
                var funcParamType = functionStoredObject.FunctionParams[i];
                var isIdNode = Parameters[i] is IdentifierNode;
                if (funcParamType.ByReference && !isIdNode)
                    throw new SemanticException($"{Parameters[i]} must be by reference.", Row);
            }
            return functionStoredObject.ReturnType;
        }

        public override Value Interpret()
        {
            var parameterListBaseTypes = new List<BaseType>();
            foreach (var parameterNode in Parameters)
                parameterListBaseTypes.Add(parameterNode.ValidateSemantic());

            //obtener la funcion que vamos a llamar
            var functionStored = (FunctionType) SymbolTable.Instance.GetFunction(FunctionName,
                Row, parameterListBaseTypes);

            SymbolTable.AddSymbolTable(FunctionLocalTable); //llenar el stack
            for (var i = 0; i < functionStored.FunctionParams.Count; i++)
            {
                var type = functionStored.FunctionParams[i].Type;
                SymbolTable.Instance.DeclareVariable(
                    functionStored.FunctionParams[i].Identifier.Name,
                    type,
                    Row);
            }
            //setear las variables de los parametros
            for (var i = 0; i < Parameters.Count; i++)
            {
                var name = functionStored.FunctionParams[i].Identifier.Name;
                var value = ((IdentifierNode) Parameters[i]).InterpretFunction();
                SymbolTable.Instance.SetVariableValue(
                    name,
                    value);
            }
            Value returnValue = new VoidValue();
            foreach (var basicStatementNode in functionStored.CompoundStatementNode.BasicStatementsList)
                if (basicStatementNode is ReturnStatementNode)
                {
                    var returnStatement = (ReturnStatementNode) basicStatementNode;
                    returnValue = returnStatement.ExpressionStatementNode.ExpressionNode.Interpret();
                }
                else
                {
                    StatementsReturn += basicStatementNode.Interpret();
                }
            SymbolTable.RemoveSymbolTable(); //limpiar el stack
            return returnValue;
        }
    }
}