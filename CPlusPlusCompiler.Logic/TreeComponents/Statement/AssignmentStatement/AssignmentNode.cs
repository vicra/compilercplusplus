﻿using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;
using CPlusPlusCompiler.Logic.TreeComponents.Expression._Parent;
using System;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.AssignmentStatement
{
    public class AssignmentNode : StatementNode
    {
        public IdentifierNode LeftValue { get; set; }
        public ExpressionStatementNode RightStatementNode { get; set; }

		public override string Interpret()
		{
			throw new NotImplementedException();
		}

		public override void ValidateSemantic()
        {
            throw new System.NotImplementedException();
        }
    }
}