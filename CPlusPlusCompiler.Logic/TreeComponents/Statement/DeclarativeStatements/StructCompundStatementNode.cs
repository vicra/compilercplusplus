using System;
using System.Collections.Generic;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SyntacticComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.VariableDeclaration;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements._Parent;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements
{
    public class StructCompundStatementNode : DeclarativeStatementNode
    {
        public List<VariableDeclarationStatementNode> VariableDeclarationListOpt { get; set; }
        public IdentifierNode IdentifierNode { get; set; }
        public List<StructInstanceNode> InitializedStructNodes { get; set; }

        public override void ValidateSemantic()
        {
            
        }

        public override BaseType GetBaseType()
        {
            throw new System.NotImplementedException();
        }

		public override string Interpret()
		{
		    foreach (var variableDeclarationStatementNode in VariableDeclarationListOpt)
		    {
		        variableDeclarationStatementNode.Interpret();
		    }
		    return "";
		}
	}
}