﻿using System;
using System.Collections.Generic;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.StatementTypes;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Compound;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Jump;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements
{
    public class FunctionDeclarationNode : StatementNode
    {
        public string FunctionName { get; set; }
        public List<ParameterNode> Parameters { get; set; }
        public CompoundStatementNode CompoundStatement { get; set; }
        public string Type { get; set; }
        public SymbolTable FunctionLocalTable = new SymbolTable();

        public override void ValidateSemantic()
        {
            var funcType = TypesTable.Instance.GetType(Type);
            List<ParameterNode> parameters = new List<ParameterNode>();

            //los inserta en el tipo
            foreach (var parameter in Parameters)
            {
                var paramType = TypesTable.Instance.GetType(parameter.Identifier.TypeSpecifier);
                parameters.Insert(0, new ParameterNode
                {
                    ByReference = parameter is ReferenceParam,
                    Type = paramType,
                    Identifier = parameter.Identifier,
                    Row = parameter.Row
                });
            }
            //agrega la funcion a la tabla de simbolos, deberia de existir table aparte para sobrecarga
            SymbolTable.Instance.DeclareFunction(FunctionName, new FunctionType()
            {
                ReturnType = funcType,
                FunctionParams = parameters,
                CompoundStatementNode = CompoundStatement
            },
            Row, parameters );

            SymbolTable.AddSymbolTable(FunctionLocalTable);

            foreach (var parameterNode in Parameters)
            {
                parameterNode.ValidateSemantic();
            }

            CompoundStatement.ValidateSemantic();
            ReturnStatementNode returnStatement = null;
            foreach (var statement in CompoundStatement.BasicStatementsList)
            {
                if (statement is ReturnStatementNode)
                {
                    returnStatement = (ReturnStatementNode)statement;
                    break;
                }
            }
            var type = returnStatement?.ExpressionStatementNode.ExpressionNode.ValidateSemantic();
            if (type == null)
            {
                type = TypesTable.Instance.GetType("void");
            }
            if (type != funcType)
            {
                throw new SemanticException($"Expected return type {this.Type} actual return type {type?.ToString()}", Row);
            }
            SymbolTable.RemoveSymbolTable();
        }

		public override string Interpret()
		{
		    return "";
		}
	}
}