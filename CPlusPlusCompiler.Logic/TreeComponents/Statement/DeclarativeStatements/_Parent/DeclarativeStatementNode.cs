﻿using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements._Parent
{
	public abstract class DeclarativeStatementNode : StatementNode
	{
        public string TypeName;
        public abstract override void ValidateSemantic();

        public abstract BaseType GetBaseType();
    }
}
