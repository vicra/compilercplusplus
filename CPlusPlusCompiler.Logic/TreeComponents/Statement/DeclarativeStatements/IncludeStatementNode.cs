﻿using System;
using System.IO;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Literals;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements._Parent;


namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements
{
    public class IncludeStatementNode : DeclarativeStatementNode
    {
        public StringLiteralNode IncludePath { get; set; }
        public override void ValidateSemantic()
        {
            var fileNameType = IncludePath.ValidateSemantic();
            if (!(fileNameType is StringType))
            {
                throw new SemanticException("Path is not String",Row);
            }
        }

        public override BaseType GetBaseType()
        {
            return IncludePath.ValidateSemantic();
        }

		public override string Interpret()
		{
		    StringValue value =  (StringValue)IncludePath.Interpret();
            var text = 
                File.ReadAllText("C:\\Users\\vicra\\Documents\\Compiladores\\compilercplusplus\\CPlusPlusCompiler.WebApplication\\"+value.Valor);
		    return text;
		}
	}

}