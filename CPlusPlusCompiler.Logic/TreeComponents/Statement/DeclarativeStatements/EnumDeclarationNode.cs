﻿using System;
using System.Collections.Generic;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.StatementTypes;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Literals;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements._Parent;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements
{
    public class EnumDeclarationNode : DeclarativeStatementNode
    {
        public IdentifierNode Identifier { get; set; }
        /// <summary>
        /// lo que va dsps de la declaracion de un enum assignadores de variables
        /// </summary>
        public List<EnumAssignerNode> EnumAssigners { get; set; }
        /// <summary>
        ///  {RANDOM, IMMEDIATE, SEARCH};
        /// </summary>
        public List<EnumeratorNode> Enumerators { get; set; }

        public override void ValidateSemantic()
        {
            if (Identifier != null)
            {
                SymbolTable.Instance.DeclareVariable(Identifier.Name, Identifier.TypeSpecifier, Row); //add enum name 
                TypesTable.Instance.RegisterType(Identifier.Name, new EnumType(), Row);
            }
            foreach (var enumerator in Enumerators)
            {
                SymbolTable.Instance.DeclareVariable(enumerator.IdentifierNode.Name, "int", Row);
                SymbolTable.Instance.AddConstant(enumerator.IdentifierNode.Name);
            }
        }

        public override BaseType GetBaseType()
        {
            TypesTable.Instance.RegisterType(TypeName, new EnumType(), Row);
            foreach (var enumNode in Enumerators)
            {
                SymbolTable.Instance.DeclareVariable(enumNode.IdentifierNode.Name, TypeName, Row);
                SymbolTable.Instance.AddConstant(enumNode.IdentifierNode.Name);
            }
            return new EnumType();
        }

		public override string Interpret()
		{
			throw new NotImplementedException();
		}
	}
}