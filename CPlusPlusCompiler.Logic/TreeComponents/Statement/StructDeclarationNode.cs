﻿using System;
using System.Collections.Generic;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SyntacticComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements._Parent;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement
{
    public class StructDeclarationNode : DeclarativeStatementNode
    {
        public IdentifierNode Identifier { get; set; }
        public StructCompundStatementNode StructCompundStatement { get; set; }
        public string Type { get; set; }

        public override void ValidateSemantic()
        {
            TypesTable.Instance.RegisterType(Identifier.Name, TypesTable.Instance.GetType("struct"),Row);
            var initializedStructNodes = StructCompundStatement.InitializedStructNodes;
            if (initializedStructNodes != null)
            {
                foreach (var initializedStructNode in initializedStructNodes)
                {
                    var name = initializedStructNode.IdentifierNode.Name;
                    var type = Identifier.Name;
                    SymbolTable.Instance.DeclareVariable(name, type, Row);
                }
            }
            StructCompundStatement.ValidateSemantic();
            //si tiene nodos de inicializacion validar el count y los tipos
        }

        public override BaseType GetBaseType()
        {
            throw new NotImplementedException();
        }

        public override string Interpret()
        {
            //se agrega el template del struct
            SymbolTable.Instance.DeclareStruct(Identifier.Name,
                Type, Row,
                StructCompundStatement.VariableDeclarationListOpt);

            if (StructCompundStatement.InitializedStructNodes != null)
            {
                foreach (var initializedStructNode in StructCompundStatement.InitializedStructNodes)
                {
                    SymbolTable.Instance.DeclareVariable(
                        initializedStructNode.IdentifierNode.Name,Identifier.Name,Row);
                    if (initializedStructNode is InitializedStructInstanceNode)
                    {
                        //crear instancia con los valores default
                        var instanceName = initializedStructNode.IdentifierNode.Name;
                        var propiedades = new List<dynamic>();

                        var literalNodes = ((InitializedStructInstanceNode)initializedStructNode).LiteralNodes;
                        foreach (var literals in literalNodes)
                        {
                            propiedades.Add(literals.Interpret().Valor);
                        }
                        var newStruct = new StructValue()
                        {
                            Propiedades = propiedades
                        };
                        SymbolTable.Instance.SetVariableValue(instanceName, newStruct);
                    }
                    else
                    {
                        //se crean las instancias con valores default
                        var instanceName = initializedStructNode.IdentifierNode.Name;
                        var propiedades = new List<dynamic>();
                        var newStruct = new StructValue()
                        {
                            Propiedades = propiedades
                        };
                        SymbolTable.Instance.SetVariableValue(instanceName, newStruct);
                    }
                }
            }
            return "";
        }
    }
}