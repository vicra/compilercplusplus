﻿namespace CPlusPlusCompiler.Logic.TreeComponents.Statement
{
    public abstract class StatementNode
    {
        public int Row { get; set; }
        public abstract void ValidateSemantic();
		public abstract string Interpret();
    }
}
