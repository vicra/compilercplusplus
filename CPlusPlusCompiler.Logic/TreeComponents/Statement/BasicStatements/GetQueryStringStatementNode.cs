﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements
{
    public class GetQueryStringStatementNode : ExpressionNode
    {
        public ExpressionNode Parameter { get; set; }
        public override BaseType ValidateSemantic()
        {
            return Parameter.ValidateSemantic();
        }

        public override Value Interpret()
        {
            return Parameter.Interpret();
        }
    }
}