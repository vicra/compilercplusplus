﻿using System;
using CPlusPlusCompiler.Logic.TreeComponents.Expression;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Iteration
{
	public class ForEachStatementNode : IterationStatementNode
	{
	    public ForExpressionStatementNode ForExpressionStatement { get; set; }

	    public override void ValidateSemantic()
	    {
	        throw new NotImplementedException();
	    }

	    public override string Interpret()
	    {
	        throw new NotImplementedException();
	    }
	}
}
