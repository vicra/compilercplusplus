﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.TreeComponents.Expression;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators.Relational;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Operators._Parents;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.VariableDeclaration;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Iteration
{
    public class ForStatementNode : IterationStatementNode
    {
		public ForExpressionStatementNode ForExpressionStatement { get; set; }

		public override string Interpret()
		{
		    ForExpressionStatement.ExpressionA.Interpret();
            var expressionB = ForExpressionStatement.ExpressionB;
            var expressionC = ForExpressionStatement.ExpressionC;

		    var returnString = "";
		    while (true)
		    {
		        if (((BoolValue)expressionB.Interpret()).Valor)
		        {
                    returnString += BasicStatement.Interpret();
		            expressionC.Interpret();
		        }
		        else
		        {
		            break;
		        }
		    }
            return returnString;
		}

        public override void ValidateSemantic()
		{
		    ForExpressionStatement.ValidateSemantic();
            BasicStatement.ValidateSemantic();
		}
    }
}