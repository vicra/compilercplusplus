﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;
using CPlusPlusCompiler.Logic.TreeComponents.Expression;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Iteration
{
    public class DoWhileStatementNode : IterationStatementNode
    {
        public ExpressionNode Condition { get; set; }

        public override string Interpret()
        {
            var returnString = "";
            do
            {
                returnString += BasicStatement.Interpret();
            } while (((BoolValue) Condition.Interpret()).Valor);
            return returnString;
        }

        public override void ValidateSemantic()
        {
            var conditionType = Condition.ValidateSemantic();
            if (!(conditionType is BoolType))
                throw new SemanticException("Condition needs to return boolean", Row);
            BasicStatement?.ValidateSemantic();
        }
    }
}