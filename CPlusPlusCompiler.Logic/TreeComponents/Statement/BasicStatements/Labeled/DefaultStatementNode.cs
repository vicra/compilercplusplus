﻿using System;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Labeled
{
    public class DefaultStatementNode : LabeledStatementNode
    {
		public override string Interpret()
		{
            var returnString = "";
            foreach (var basicStatementNode in BasicStatementList)
            {
                returnString += basicStatementNode.Interpret();
            }
            return returnString;
        }

		public override void ValidateSemantic()
        {
            foreach (var caseStatement in BasicStatementList)
                caseStatement.ValidateSemantic();
        }
    }
}