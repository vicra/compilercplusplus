﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.VariableDeclaration
{
    public class VariableDeclarationAssignmentNode : VariableDeclarationNode
    {
        public IdentifierNode IdentifierNode { get; set; }
        public ExpressionNode Value { get; set; }

        public override BaseType ValidateSemantic()
        {
            var idType = IdentifierNode.ValidateSemantic();
            var valueType = Value.ValidateSemantic();
            if (idType != valueType)
                throw new SemanticException("Cannot assign ", Row);
            return valueType;
        }

        public override Value Interpret()
        {
            SymbolTable.Instance.SetVariableValue(IdentifierNode.Name, Value.Interpret());
            return Value.Interpret();
        }
    }
}