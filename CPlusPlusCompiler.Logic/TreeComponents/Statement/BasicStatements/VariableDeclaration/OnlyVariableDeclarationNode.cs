﻿using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.VariableDeclaration
{
    public class OnlyVariableDeclarationNode : VariableDeclarationNode
    {
        public IdentifierNode IdentifierNode { get; set; }
        public override BaseType ValidateSemantic()
        {
            return IdentifierNode.ValidateSemantic();
        }

        public override Value Interpret()
        {
            return IdentifierNode.Interpret();
        }
    }
}