﻿using System;
using System.Collections.Generic;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.VariableDeclaration
{
    public class VariableDeclarationStatementNode : BasicStatementNode
    {
        public bool IsConst { get; set; }
        public int PointerLevel { get; set; }
        public List<VariableDeclarationNode> VariableDeclarationList { get; set; }
        public string Type { get; set; }

        public override void ValidateSemantic()
        {
            var baseType = TypesTable.Instance.GetType(Type);
            foreach (var variableDeclarationNode in VariableDeclarationList)
                if (variableDeclarationNode is OnlyVariableDeclarationNode)
                {
                    if (TypesTable.Instance.GetType(Type) is ArrayType)
                    {
                        SymbolTable.Instance.DeclareVariable(
                            ((OnlyVariableDeclarationNode) variableDeclarationNode).IdentifierNode.Name,
                            "array", Row);
                        TypesTable.Instance.RegisterType(
                            ((OnlyVariableDeclarationNode) variableDeclarationNode).IdentifierNode.Name,
                            TypesTable.Instance.GetType(Type), Row);
                    }
                    else
                    {
                        SymbolTable.Instance.DeclareVariable(
                            ((OnlyVariableDeclarationNode) variableDeclarationNode).IdentifierNode.Name,
                            Type, Row);
                    }
                }
                else if (variableDeclarationNode is VariableDeclarationAssignmentNode)
                {
                    var varDecl = (VariableDeclarationAssignmentNode) variableDeclarationNode;
                    var idType = TypesTable.Instance.GetType(Type);
                    var valueType = varDecl.Value.ValidateSemantic();
                    if (idType != valueType)
                        throw new SemanticException($"Value {valueType} cannot be assigned to a type {idType}  ", Row);
                    SymbolTable.Instance.DeclareVariable(varDecl.IdentifierNode.Name, Type, Row);
                }
        }

        public override string Interpret()
        {
            foreach (var variableDeclarationNode in VariableDeclarationList)
            {
                if (variableDeclarationNode is VariableDeclarationAssignmentNode)
                {
                    var varDeclAssign = (VariableDeclarationAssignmentNode) variableDeclarationNode;
                    SymbolTable.Instance.SetVariableValue(
                        ((VariableDeclarationAssignmentNode)variableDeclarationNode).IdentifierNode.Name,
                        varDeclAssign.Value.Interpret());
                }
            }
            return "";
        }
    }
}