﻿using System;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.HtmlContent
{
    public class HtmlContentStatementNode : BasicStatementNode
    {
        public string Content { get; set; }

		public override string Interpret()
		{
			return Content;
		}

		public override void ValidateSemantic()
        {
            // no neccesary implemmentation
        }
    }
}