﻿using System.Collections.Generic;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents
{
    public abstract class LabeledStatementNode : BasicStatementNode
    {
        public List<BasicStatementNode> BasicStatementList { get; set; }

        public LabeledStatementNode()
        {
            BasicStatementList = new List<BasicStatementNode>();
        }
    }
}
