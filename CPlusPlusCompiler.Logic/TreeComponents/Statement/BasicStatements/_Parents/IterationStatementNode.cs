﻿using System.Collections.Generic;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents
{
    public abstract class IterationStatementNode : BasicStatementNode
    {
        public BasicStatementNode BasicStatement { get; set; }
    }
}
