﻿namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents
{
    public abstract class BasicStatementNode : StatementNode
    {
        public abstract override void ValidateSemantic();
    }
}
