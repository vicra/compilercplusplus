﻿using System.Collections.Generic;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Compound
{
    public class CompoundStatementNode : BasicStatementNode
    {
        public SymbolTable LocalTable = new SymbolTable();

        public CompoundStatementNode()
        {
            BasicStatementsList = new List<BasicStatementNode>();
        }

        public List<BasicStatementNode> BasicStatementsList { get; set; }

        public override void ValidateSemantic()
        {
            SymbolTable.AddSymbolTable(LocalTable);
            foreach (var basicStatementNode in BasicStatementsList)
                basicStatementNode.ValidateSemantic();
            SymbolTable.RemoveSymbolTable();
        }

        public override string Interpret()
        {
            var returnString = "";
            foreach (var basicStatementNode in BasicStatementsList)
                returnString += basicStatementNode.Interpret();
            return returnString;
        }
    }
}