﻿using System;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Jump
{
    public class ContinueStatementNode : BasicStatementNode
    {
		public override string Interpret()
		{
		    return "";
		}

		public override void ValidateSemantic()
        {
            // no implementation needed
        }
    }
}