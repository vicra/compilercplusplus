﻿using System;
using CPlusPlusCompiler.Logic.TreeComponents.Expression._Parent;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Jump
{
    public class ReturnStatementNode : BasicStatementNode
    { 
        public ReturnStatementNode(ExpressionStatementNode expressionStatementNode)
		{
			ExpressionStatementNode = expressionStatementNode;
		}

		public ExpressionStatementNode ExpressionStatementNode { get; set; }

		public override string Interpret()
		{
		    return ExpressionStatementNode.Interpret();
		}

		public override void ValidateSemantic()
        {
            ExpressionStatementNode.ValidateSemantic();
        }
    }
}