﻿using System;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;
using CPlusPlusCompiler.Logic.TreeComponents.Expression;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Selection
{
    public class IfStatementNode : BasicStatementNode
    {
        public ExpressionNode Condition { get; set; }
        public BasicStatementNode Else { get; set; }
        public BasicStatementNode BasicStatement { get; set; }
        public override void ValidateSemantic()
        {
            var conditionType = Condition.ValidateSemantic();
            if (!(conditionType is BoolType))
            {
                throw new SemanticException("Condition is not a boolean expression", Row);
            }
            else
            {
                BasicStatement?.ValidateSemantic();
                Else?.ValidateSemantic();
            }
        }

		public override string Interpret()
		{
		    var conditionValue = Condition.Interpret();
		    var returnString = "";
		    if (conditionValue.Valor)
		    {
		        returnString = BasicStatement.Interpret();
		    }
		    else
		    {
		        returnString = Else?.Interpret();
		    }
		    return returnString;
		}
	}
}
