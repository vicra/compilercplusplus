﻿using System;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;
using CPlusPlusCompiler.Logic.TreeComponents.Expression;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements._Parents;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements.Selection
{
    public class SwitchStatementNode : BasicStatementNode
    {
        public ExpressionNode Expression { get; set; }

        public BasicStatementNode BasicStatement { get; set; }

        public override void ValidateSemantic()
        {
            var expressionType = Expression.ValidateSemantic();
            if (expressionType is BoolType ||
                expressionType is CharType ||
                expressionType is DateType ||
                expressionType is FloatType ||
                expressionType is IntegerType ||
                expressionType is StringType)
                BasicStatement.ValidateSemantic();
            else
                throw new SemanticException($"{expressionType} not supported switch statement", Row);
        }

        public override string Interpret()
        {
            throw new NotImplementedException();
        }
    }
}