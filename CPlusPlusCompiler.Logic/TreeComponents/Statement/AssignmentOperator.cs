﻿using System;
using CPlusPlusCompiler.Logic.InterpretComponents;
using CPlusPlusCompiler.Logic.SemanticComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression;
using CPlusPlusCompiler.Logic.TreeComponents.Expression.Id;

namespace CPlusPlusCompiler.Logic.TreeComponents.Statement
{
    public class AssignmentOperator : ExpressionNode
    {
        public ExpressionNode ExpressionNode { get; set; }
        public ExpressionNode AssignmentExpressionNode { get; set; }

        public override BaseType ValidateSemantic()
        {
			var leftType = ExpressionNode.ValidateSemantic();
			var rightType = AssignmentExpressionNode.ValidateSemantic();
			if (leftType != rightType)
			{
				throw new SemanticException($"Data type {rightType} type cant be assigned to a {leftType} type", Row);
			}
			return leftType;
        }

        public override Value Interpret()
        {
            SymbolTable.Instance.SetVariableValue(((IdentifierNode)ExpressionNode).Name, AssignmentExpressionNode.Interpret());
            return AssignmentExpressionNode.Interpret();
        }
    }
}