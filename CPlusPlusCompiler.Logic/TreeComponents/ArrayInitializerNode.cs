﻿using CPlusPlusCompiler.Logic.TreeComponents.Expression;

namespace CPlusPlusCompiler.Logic.TreeComponents
{
    public class ArrayInitializerNode
    {
        public ArrayInitializerPrimeNode ArrayInitializerPrimeNode { get; set; }

        public ExpressionNode ExpressionNode { get; set; }
    }

    public class ArrayInitializerPrimeNode
    {
        public ArrayInitializerNode ArrayInitializerNode { get; set; }

        public ArrayInitializerBiPrimeNode ArrayInitializerBiPrimeNode { get; set; }
    }

    public class ArrayInitializerBiPrimeNode
    {
        public ArrayInitializerPrimeNode ArrayInitializerPrimeNode { get; set; }
    }
}