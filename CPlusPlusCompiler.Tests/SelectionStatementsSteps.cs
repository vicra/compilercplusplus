﻿using System;
using CPlusPlusCompiler.Logic.LexerComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.DataTypes;
using CPlusPlusCompiler.Logic.SyntacticComponents;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace CPlusPlusCompiler.Tests
{
    [Binding]
    public class SelectionStatementsSteps
    {
        private Lexer _lexer;

        [Given]
        public void GivenTheFollowingSelectionStatement_P0(string statement)
        {
            _lexer = new Lexer(statement);
            BaseType _base = new BoolType();
        }
        
        [When]
        public void WhenValidateSemantic()
        {
            var parser = new Parser(_lexer);
            var code = parser.Parse();
            foreach (var stmt in code)
            {
                stmt.ValidateSemantic();
            }
        }
        
        [Then]
        public void ThenTheCodeShouldPassWithNoErrors()
        {
            Assert.AreEqual(1,1);
        }
    }
}
