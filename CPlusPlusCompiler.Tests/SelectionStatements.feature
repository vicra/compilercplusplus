﻿Feature: SelectionStatements
	In order to avoid errors on selection statements
	As a compiler programmer
	I want to validate semantic errors

Scenario: ifstatment
	Given the following selection statement if(1 < 2 );
	When validate semantic
	Then the code should pass with no errors
