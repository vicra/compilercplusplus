﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Routing;
using CPlusPlusCompiler.Logic.LexerComponents;
using CPlusPlusCompiler.Logic.SemanticComponents.Tables;
using CPlusPlusCompiler.Logic.SemanticComponents.TypesComponents.StatementTypes;
using CPlusPlusCompiler.Logic.SyntacticComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression;
using CPlusPlusCompiler.Logic.TreeComponents.Expression._Parent;
using CPlusPlusCompiler.Logic.TreeComponents.Statement;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.BasicStatements;
using CPlusPlusCompiler.Logic.TreeComponents.Statement.DeclarativeStatements;

namespace CPlusPlusCompiler.WebApplication.App_Start
{
    public class CPlusPlusCompilerHandler : IHttpHandler
    {
        public RequestContext RequestContext { get; set; }

        public CPlusPlusCompilerHandler(RequestContext requestContext)
        {
            RequestContext = requestContext;
        }
        public void ProcessRequest(HttpContext context)
        {
            var text = File.ReadAllText("C:\\Users\\vicra\\Documents\\Compiladores\\compilercplusplus\\CPlusPlusCompiler.WebApplication\\File.html");
            var lex = new Lexer(text);
            var parser = new Parser(lex);
            var code = parser.Parse();

            List<StatementNode> codeInclude = new List<StatementNode>();
            string includeCode = "<%";
            foreach (var statement in code)
            {
                if (statement is IncludeStatementNode)
                {
                    statement.ValidateSemantic();
                    includeCode += statement.Interpret();
                }
            }
            includeCode += "%>";
            var lexInclude = new Lexer(includeCode);
            var parserInclude = new Parser(lexInclude);
            codeInclude = parserInclude.Parse();

            //agregar el codigo del include al codigo padre
            codeInclude.Reverse();
            foreach (var codeIncludeStatement in codeInclude)
            {
                code.Insert(0, codeIncludeStatement);
            }

            /*  VALIDAR SEMANTICA  */
            foreach (var statement in code)
            {
                if (!(statement is IncludeStatementNode))
                {
                    statement.ValidateSemantic();
                }
            }

            /*  INTERPRETAR  */
            string serverResponse = "";
            foreach (var statement in code)
            {
                if (!(statement is IncludeStatementNode))
                {
                    serverResponse += statement.Interpret();
                }
                if (statement is ExpressionStatementNode)
                {
                    if (((ExpressionStatementNode)statement).ExpressionNode is CallFunctionNode)
                    {
                        serverResponse += ((CallFunctionNode)((ExpressionStatementNode)statement).ExpressionNode).StatementsReturn;
                    }
                }
            }
            context.Response.Write(serverResponse);
        }

        private static void ImplemenatacionMultiplesPasadas(List<StatementNode> code)
        {
            foreach (var statementNode in code)
            {
                if (statementNode is FunctionDeclarationNode)
                {
                    var function = (FunctionDeclarationNode) statementNode;
                    var funcType = TypesTable.Instance.GetType(function.Type);

                    List<ParameterNode> parameters = new List<ParameterNode>();

                    //los inserta en el tipo
                    foreach (var parameter in function.Parameters)
                    {
                        var paramType = TypesTable.Instance.GetType(parameter.Identifier.TypeSpecifier);
                        parameters.Insert(0, new ParameterNode
                        {
                            ByReference = parameter is ReferenceParam,
                            Type = paramType,
                            Identifier = parameter.Identifier,
                            Row = parameter.Row
                        });
                    }

                    SymbolTable.Instance.DeclareFunction(function.FunctionName, new FunctionType()
                    {
                        ReturnType = funcType,
                        FunctionParams = parameters,
                        CompoundStatementNode = function.CompoundStatement
                    }, function.Row, parameters);
                }
            }
        }

        public bool IsReusable => false;
    }
}