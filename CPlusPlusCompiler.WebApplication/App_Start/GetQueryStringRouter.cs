﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace CPlusPlusCompiler.WebApplication.App_Start
{
    public class GetQueryStringRouter : IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new GetQueryStringHandler(requestContext);
        }
    }
}