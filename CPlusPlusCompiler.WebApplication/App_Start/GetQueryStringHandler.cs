﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.WebPages;
using CPlusPlusCompiler.Logic.LexerComponents;
using CPlusPlusCompiler.Logic.SyntacticComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression._Parent;
using CPlusPlusCompiler.Logic.TreeComponents.Statement;

namespace CPlusPlusCompiler.WebApplication.App_Start
{
    public class GetQueryStringHandler : IHttpHandler
    {
        public RequestContext RequestContext { get; set; }

        public GetQueryStringHandler(RequestContext requestContext)
        {
            RequestContext = requestContext;
        }
        public void ProcessRequest(HttpContext context)
        {
            var value1 = context.Request["param"];
            string text = "<% if(1<2){" +
                       "string a = getquerystring(\"" + value1 + "\");" +
                       "print(a); " +
                       "}%>";

            var lex = new Lexer(text);
            var parser = new Parser(lex);
            var code = parser.Parse();
            foreach (var statement in code)
            {
                statement.ValidateSemantic();
            }
            string serverResponse = "";
            foreach (var statement in code)
            {
                serverResponse += statement.Interpret();
                if (statement is ExpressionStatementNode)
                {
                    serverResponse += 
                        ((CallFunctionNode)((ExpressionStatementNode)statement)
                        .ExpressionNode).StatementsReturn;
                }
            }
            context.Response.Write(serverResponse);
        }

        public bool IsReusable { get; } = true;
    }
}