﻿<html>
	<body>
		<div>
			<%
				int i;
				for (i = 0; i < 10; i++)
				{
			%>
			<div>
				<%	print (i); %>
			</div>
			<%
				}
			%>
		</div>
	</body>
</html>
/**************************************/
<html>
	<%
		#include "unistd.h"
	%>
</html>
/**************************************/
<html>
	<%
		struct book {
			int a; int b;
			char **** arr[4][5];
		};
	%>
</html>
/**************************************/
<html>
	<%
		enum nose {
			nose = 1,
			nose2 = 2,
			nose3 = 3
		};
	%>
</html>
/**************************************/
<html>
	<%
		void SDL_PathFullName(char* dst, char* path[][4]) {
			a = b = c++;
			GetFullPathName(path, MAX_PATH, dst, GetFullPathName(path.x->y[5]++, MAX_PATH, dst, NULL));
		}
	%>
</html>
/**************************************/
<html>
	<%
		void SDL_PathFullName(char* dst, /*const*/ char* path) {
			char* ret = realpath(path, dst);
		}
	%>
</html>
/**************************************/
<html>
	<%
		void SDL_PathFileName(char* dst, char* path) {
			int i = strlen(path);
			int ext_loc = 0;
			while (i > 0) {
				if (path[i] == '/') { break; }
				if (path[i] == '\\') { break; }
				if (path[i] == '.') { ext_loc = i; }
				i--;
			}
			const char* file = path + i + 1;
			int len = ext_loc - i - 1;
			strncpy(dst, file, len);
			dst[len] = '\0';
		}
	%>
</html>
/**************************************/
<html>
	<%
		void SDL_PathFileExtension(char* dst, char* path) {
			int ext_len = 0;
			int i = strlen(path);
			while (i >= 0) {
				if (path[i] != '.') { ext_len++; }
				if (path[i] == '.') { break; }
				i--;
			}
			int prev = strlen(path) - ext_len + 1;
			const char* f_ext = path + prev;
			strcpy(dst, f_ext);
		}
	%>
</html>
/**************************************/
<html>
	<%
		void SDL_PathFileLocation(char* dst, char* path) {
			int i = strlen(path);
			while (i > 0) {
				if (path[i] == '/') { break; }
				if (path[i] == '\\') { break; }
				i--;
			}
			i++;
			strncpy(dst, path, i);
			dst[i] = '\0';
		}
	%>
</html>
/**************************************/
<html>
	<%
		void SDL_PathParentDirectory(char* dst, char* path) {
			int i = strlen(path) - 1;
			while (i > 0) {
				if (path[i] == '/') { break; }
				if (path[i] == '\\') { break; }
				i--;
			}
			strncpy(dst, path, i);
			dst[i] = '\0';
		}
	%>
</html>