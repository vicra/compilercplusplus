﻿using System.IO;
using CPlusPlusCompiler.Logic.LexerComponents;
using CPlusPlusCompiler.Logic.SyntacticComponents;
using CPlusPlusCompiler.Logic.TreeComponents.Expression._Parent;
using CPlusPlusCompiler.Logic.TreeComponents.Statement;

namespace CPlusPlusCompiler.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var text = File.ReadAllText("../../File.html");
            var lex = new Lexer(text);
            var parser = new Parser(lex);
            var code = parser.Parse();
            foreach (var statement in code)
                statement.ValidateSemantic();
            var serverResponse = "";
            foreach (var statement in code)
            {
                serverResponse += statement.Interpret();
                if (statement is ExpressionStatementNode)
                    serverResponse += ((CallFunctionNode) ((ExpressionStatementNode) statement).ExpressionNode).StatementsReturn;
            }
            System.Console.Write(serverResponse);
            System.Console.ReadKey();
        }
    }
}